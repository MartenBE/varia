<!--
Copyright (C) 2020 Martijn Saelens

SPDX-License-Identifier: MIT
-->

# Varia

[![pipeline status](https://gitlab.com/MartenBE/varia/badges/master/pipeline.svg)](https://gitlab.com/MartenBE/varia/-/commits/master)
[![coverage report](https://gitlab.com/MartenBE/varia/badges/master/coverage.svg)](https://gitlab.com/MartenBE/varia/-/commits/master)
[![REUSE status](https://api.reuse.software/badge/gitlab.com/martenbe/varia)](https://api.reuse.software/info/gitlab.com/martenbe/varia)

### Table of Contents

* [Goals](#goals)
* [Content status](#content-status)
* [To do](#to-do)
* [Contributing](#contributing)
* [License](#license)
* [Used libraries](#used-libraries)
* [FAQ](#faq)

---

### Goals

* Clean code to provide an example of how to implement an algorithm or calculation.
* Provide unit tests for algorithms, including as much edge cases as possible (**[help wanted!](./CONTRIBUTING.md)**).
* Useful library to perform algorithms and calculations, although readability is more important than speed.

### Content status

#### Data structures

Algorithm                                                                                                       | File                                                                                      | Unit tests                                                                                    | Remarks
--------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------
[Binary Heap](https://en.wikipedia.org/wiki/Binary_heap)                                                        | [binary-heap.hpp](./include/tree/binary-heap.hpp)                                         | [priority-queue-test.cpp](./tests/priority-queue-test.cpp)                                    | Contains a `heapify` method which can be used on arrays and vectors.
[Binary Search Tree](https://en.wikipedia.org/wiki/Binary_search_tree)                                          | [binary-search-tree.hpp](./include/tree/binary-search-tree.hpp)                           | [map-test.cpp](./tests/map-test.cpp)                                                          | Does not allow duplicates.
[Binomial Heap](https://en.wikipedia.org/wiki/Binomial_heap)                                                    | [binomial-heap.hpp](./include/tree/binomial-heap.hpp)                                     | [priority-queue-test.cpp](./tests/priority-queue-test.cpp)                                    | (TO DO) Should change internal representation of Binomial Tree to [left-child right-sibling binary tree](https://en.wikipedia.org/wiki/Left-child_right-sibling_binary_tree).
Dumb BigInteger                                                                                                 | [dumb-biginteger.hpp](./include/tools/dumb-biginteger.hpp)                                | [biginteger-test.cpp](./tests/biginteger-test.cpp)                                            | Not efficient and does not have a lot of features. Currently only supports the `+` and `-` operators.
[Graph](https://en.wikipedia.org/wiki/Graph_(discrete_mathematics))                                             | [graph.hpp](./include/graph/graph.hpp)                                                    | [graph-test.cpp](./tests/graph-test.cpp)                                                      | /
[Matrix](https://en.wikipedia.org/wiki/Matrix_(mathematics))                                                    | [matrix.hpp](./include/matrix/matrix.hpp)                                                 | [matrix-test.cpp](./tests/matrix-test.cpp)                                                    | /

#### Graph algorithms

Algorithm                                                                                                       | File                                                                                      | Unit tests                                                                                    | Remarks
--------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------
[Breadth First Search](https://en.wikipedia.org/wiki/Breadth-first_search)                                      | [path-search.hpp](./include/graph/path-search.hpp)                                        | [path-search-test.cpp](./tests/path-search-test.cpp)                                          | /
[Depth First Search](https://en.wikipedia.org/wiki/Depth-first_search) (iterative)                              | [path-search.hpp](./include/graph/path-search.hpp)                                        | [path-search-test.cpp](./tests/path-search-test.cpp)                                          | /
[Depth First Search](https://en.wikipedia.org/wiki/Depth-first_search) (recursive)                              | [path-search.hpp](./include/graph/path-search.hpp)                                        | [path-search-test.cpp](./tests/path-search-test.cpp)                                          | /
[Depth First Search Traversal](https://en.wikipedia.org/wiki/Depth-first_search) (recursive)                    | [path-search.hpp](./include/graph/path-search.hpp)                                        | [path-search-test.cpp](./tests/path-search-test.cpp)                                          | Goes over all vertices in the graph. Can be given a function to execute in pre- or postorder. Inorder is not relevant as there is no guarantee the DAG is a BST.
[Dijkstra](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm)                                                | [dijkstra.hpp](./include/graph/dijkstra.hpp)                                              | [single-source-shortest-path-test.cpp](./tests/single-source-shortest-path-test.cpp)          | /
[Floyd-Warshall](https://en.wikipedia.org/wiki/Floyd%E2%80%93Warshall_algorithm)                                | [floyd-warshall.hpp](./include/graph/floyd-warshall.hpp)                                  | [all-pairs-shortest-paths-test.cpp](./tests/all-pairs-shortest-paths-test.cpp)                | Currently creates a matrix in every step. Also only returns the distances, not the paths (TO DO).
[Kruskal](https://en.wikipedia.org/wiki/Kruskal%27s_algorithm)                                                  | [kruskal.hpp](./include/graph/kruskal.hpp)                                                | [minimum-spaning-tree-test.cpp](./tests/minimum-spaning-tree-test.cpp)                        | Uses the Disjoint-set datastructure using forests.
[Prim](https://en.wikipedia.org/wiki/Prim%27s_algorithm)                                                        | [prim.hpp](./include/graph/prim.hpp)                                                      | [minimum-spaning-tree-test.cpp](./tests/minimum-spaning-tree-test.cpp)                        | There is no support for a forest of minimum-spanning-trees. The algorithm currently uses the smallest vertex id of the graph to start.
[Strongly Connected Components](https://en.wikipedia.org/wiki/Kosaraju%27s_algorithm) (Kosaraju)                | [strongly-connected-components.hpp](./include/graph/strongly-connected-components.hpp)    | [strongly-connected-components-test.cpp](./tests/strongly-connected-components-test.cpp)      | (TO DO) Implement Tarjan's algorithm.
[Topological Sort](https://en.wikipedia.org/wiki/Topological_sorting) (using depth-first-search traversal)      | [topological-sort.hpp](./include/graph/topological-sort.hpp)                              | [topological-sort-test.cpp](./tests/topological-sort-test.cpp)                                | /
[Topological Sort](https://en.wikipedia.org/wiki/Topological_sorting) (using kahn's algorithm)                  | [topological-sort.hpp](./include/graph/topological-sort.hpp)                              | [topological-sort-test.cpp](./tests/topological-sort-test.cpp)                                | /

#### Search algorithms

Algorithm                                                                                                       | File                                                                                      | Unit tests                                                                                    | Remarks
--------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------
[Binary Search](https://en.wikipedia.org/wiki/Binary_search_algorithm) (iterative)                              | [sorted-array-search.hpp](./include/search/sorted-array-search.hpp)                       | [sorted-array-search-test.cpp](./tests/sorted-array-search-test.cpp)                          | /
[Binary Search](https://en.wikipedia.org/wiki/Binary_search_algorithm) (recursive)                              | [sorted-array-search.hpp](./include/search/sorted-array-search.hpp)                       | [sorted-array-search-test.cpp](./tests/sorted-array-search-test.cpp)                          | /
[Linear Search](https://en.wikipedia.org/wiki/Linear_search)                                                    | [sorted-array-search.hpp](./include/search/sorted-array-search.hpp)                       | [sorted-array-search-test.cpp](./tests/sorted-array-search-test.cpp)                          | /

#### Set algorithms

Algorithm                                                                                                       | File                                                                                      | Unit tests                                                                                    | Remarks
--------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------
[Disjoint-set datastructure](https://en.wikipedia.org/wiki/Disjoint-set_data_structure) (using forests)         | [disjoint-sets-forests.hpp](./include/sets/disjoint-sets-forests.hpp)                     | [disjoint-sets-test.cpp](./tests/disjoint-sets-test.cpp)                                      | Also known as the union-find problem. Uses the forests approach using the union by rank and path compression heuristics.
[Disjoint-set datastructure](https://en.wikipedia.org/wiki/Disjoint-set_data_structure) (using linked lists)    | [disjoint-sets-linked-lists.hpp](./include/sets/disjoint-sets-linked-lists.hpp)           | [disjoint-sets-test.cpp](./tests/disjoint-sets-test.cpp)                                      | Also known as the union-find problem. Uses the linked list approach with the weighted-union heuristic. Instead of placing the pointers to the list object inside the list nodes, I've used an `unordered_map`. Also, empty sets are not cleaned up (TO DO).

#### Shuffle algorithms

Algorithm                                                                                                       | File                                                                                      | Unit tests                                                                                    | Remarks
--------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------
[Durstenfeld shuffle](https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle)                               | [shuffle.hpp](./include/random/shuffle.hpp)                                               | [shuffle-test.cpp](./tests/shuffle-test.cpp)                                                  | Often referred to as the Fisher–Yates shuffle (the original manual method).
[Naieve shuffle](https://en.wikipedia.org/wiki/Shuffling)                                                       | [shuffle.hpp](./include/random/shuffle.hpp)                                               | [shuffle-test.cpp](./tests/shuffle-test.cpp)                                                  | This is biased, so not full random as often is expected [2]. A detection function for bias is implemented in the unit test code.

#### Sorting algorithms

Algorithm                                                                                                       | File                                                                                      | Unit tests                                                                                    | Remarks
--------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------
[Bubble Sort](https://en.wikipedia.org/wiki/Bubble_sort)                                                        | [bubblesort.hpp](./include/sorting/bubblesort.hpp)                                        | [sorting-test.cpp](./tests/sorting-test.cpp)                                                  | /
[Counting Sort](https://en.wikipedia.org/wiki/Counting_sort)                                                    | [countingsort.hpp](./include/sorting/countingsort.hpp)                                    | [sorting-test.cpp](./tests/sorting-test.cpp)                                                  | /
[Heapsort](https://en.wikipedia.org/wiki/Heapsort)                                                              | [heapsort.hpp](./include/sorting/heapsort.hpp)                                            | [sorting-test.cpp](./tests/sorting-test.cpp)                                                  | /
[Insertion Sort](https://en.wikipedia.org/wiki/Insertion_sort)                                                  | [insertionsort.hpp](./include/sorting/insertionsort.hpp)                                  | [sorting-test.cpp](./tests/sorting-test.cpp)                                                  | /
[Merge Sort](https://en.wikipedia.org/wiki/Merge_sort)                                                          | [mergesort.hpp](./include/sorting/mergesort.hpp)                                          | [sorting-test.cpp](./tests/sorting-test.cpp)                                                  | (TO DO) Implement variant without excessive copying by alternating the given and auxiliary array.
[Quicksort](https://en.wikipedia.org/wiki/Quicksort)                                                            | [quicksort.hpp](./include/sorting/quicksort.hpp)                                          | [sorting-test.cpp](./tests/sorting-test.cpp)                                                  | Uses Hoare partitioning. (TO DO) Also implement Lomuto partitioning.
[Selection Sort](https://en.wikipedia.org/wiki/Selection_sort)                                                  | [selectionsort.hpp](./include/sorting/selectionsort.hpp)                                  | [sorting-test.cpp](./tests/sorting-test.cpp)                                                  | /

#### String algorithms

Algorithm                                                                                                       | File                                                                                      | Unit tests                                                                                    | Remarks
--------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------
[Knuth-Morris-Pratt](https://en.wikipedia.org/wiki/Knuth%E2%80%93Morris%E2%80%93Pratt_algorithm)                | [knuth-morris-pratt.hpp](./include/string/knuth-morris-pratt.hpp)                         | [string-search-test.cpp](./tests/string-search-test.cpp)                                      | /
[Longest Common Subsequence](https://en.wikipedia.org/wiki/Longest_common_subsequence_problem)                  | [longest-common-substring.hpp](./include/string/longest-common-substring.hpp)                    | [longest-common-substring-test.cpp](./tests/longest-common-substring-test.cpp)                | Only finds a single candidate, not all candidates (this would need a non-polynomial backtrack function).
[Naieve](https://en.wikipedia.org/wiki/String-searching_algorithm#Na%C3%AFve_string_search)                     | [naieve-string-search.hpp](./include/string/naieve-string-search.hpp)                     | [string-search-test.cpp](./tests/string-search-test.cpp)                                      | /

* [1] Thomas H. Cormen, Charles E. Leiserson, Ronald L. Rivest, and Clifford Stein. 2009. Introduction to Algorithms, Third Edition (3rd. ed.). The MIT Press.
* [2] https://blog.codinghorror.com/the-danger-of-naivete/

### To do

Structural:

* Benchmarking
* Implement fuzzy testing
* Find a way to unit test the asserts
* Output nlohmann::json with 4-space width if catch2 REQUIRE fails
* Consolidate the `path-search` and `visit-nodes` functionalities

Algorithms:

* Hashtables (chaining, open addressing)
* Red Black trees (bottom-up, top-down)
* Splay trees (bottom-up, top-down)
* B-Trees
* Quad trees
* K-D trees
* Pairing Heaps
* Eulercircuits
* Bellman-Ford
* Johnson's algorithm
* Flow networks
* Stable marriage
* Tries
* Boyer Moore
* Karp Rabin
* Shift And
* Suffix trees
* Suffix arrays
* Simulated Annealing
* Neural networks

### Contributing

Feel free to create issues or pull requests! See [CONTRIBUTING.md](./CONTRIBUTING.md) for more information.

### License

This project adheres to the [REUSE](https://reuse.software/) licensing guidelines [1]. The licenses used for this library can be found in [LICENSES](./LICENSES). Each file in this repository has a comment declaring the license used for that file. The year in the copyright notice is the year of initial publication of the file and is not updated when the file is modified. See [CONTRIBUTING.md](./CONTRIBUTING.md) for more information. Currently, all files are licensed under the "[MIT](./LICENSES/MIT.txt)" license [2][3][4][5].

The licenses are linted by the [REUSE software](https://reuse.software/) [1]. For example to add a license:

```bash
reuse annotate --copyright "<your_name>" --year <current_year> --license MIT <file> --copyright-style string-c
```

* [1] https://reuse.software/
* [2] https://mit-license.org/
* [3] https://opensource.org/licenses/MIT
* [4] https://spdx.org/licenses/MIT.html
* [5] https://tldrlegal.com/license/mit-license

### Used libraries

Library | License
------- | -------
[catchorg/Catch2](https://github.com/catchorg/Catch2.git) | [Boost - Version 1.0](https://github.com/catchorg/Catch2/blob/master/LICENSE.txt)
[nlohmann/json](https://github.com/nlohmann/json.git) | [MIT](https://github.com/nlohmann/json/blob/develop/LICENSE.MIT)
[vincentlaucsb/csv-parser](https://github.com/vincentlaucsb/csv-parser.git) | [MIT](https://github.com/vincentlaucsb/csv-parser/blob/master/LICENSE)
[alandefreitas/matplotplusplus](https://github.com/alandefreitas/matplotplusplus) | [MIT](https://github.com/alandefreitas/matplotplusplus/blob/master/LICENSE)

### FAQ

##### Why C++?

I just personally like C++ more than the other languages, so I use it here to sharpen my C++ skills. If you need a library for production code, this is most certainly not the one you want. There are no optimizations present. This is mainly an educational project.

##### What is the deal with `std::ptrdiff_t` and `std::ssize`?

The use of unsigned integers for array sizes (`std::size_t`) is largely considered a mistake by prominent names in the C++ landscape [1][2][3]. Mixing signed and unsigned integers can lead to weird and difficult to debug bugs. Therefore we use `std::ssize()` instead of `.size()` and `std::size()` which returns a signed integer. `std::ptrdiff_t` is considered to be te signed counterpart of `std::size_t` [4] and therefore we use `std::ptrdiff_t`.

* [1] [https://channel9.msdn.com/Events/GoingNative/2013/Interactive-Panel-Ask-Us-Anything, the discussions are at 12:12-13:08, 42:40-45:26, and 1:02:50-1:03:15.](https://channel9.msdn.com/Events/GoingNative/2013/Interactive-Panel-Ask-Us-Anything)
* [2] [https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#es107-dont-use-unsigned-for-subscripts-prefer-gslindex](https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#es107-dont-use-unsigned-for-subscripts-prefer-gslindex)
* [3] [https://www.learncpp.com/cpp-tutorial/unsigned-integers-and-why-to-avoid-them/](https://www.learncpp.com/cpp-tutorial/unsigned-integers-and-why-to-avoid-them/)
* [4] [https://en.cppreference.com/w/cpp/types/std::ptrdiff_t](https://en.cppreference.com/w/cpp/types/std::ptrdiff_t)

##### What is the meaning of the following uncommon file extensions?

* `.dot`: [Graphviz dot](https://graphviz.org/) files for drawing graphs
* `.plt`: [Gnuplot](http://www.gnuplot.info/) data files for drawing plots

##### Why do you put the function bodies in the header files and not in a `.cpp` file?

Because of those pesky templates, that's why! Trying to separate template headers into `.hpp` and `.cpp` files is a frustrating experience and reduces the readibility of the code (which I find more important than compile times at this point.). As soon as C++ 20 modules are usable in GCC and Clang, I'll check into that!

Inside `.hpp` with template classes, I define the bodies of the functions inside the class to prevent errors with overloading friend operators. See [1] for more information about this subject.

Currently, the focus lies more on to make it work than optimizing compilation times. Therefore, we put everything in the header files. Perhaps this will be addressed in the future.

* [1] https://stackoverflow.com/questions/4660123/overloading-friend-operator-for-template-class/4661372#4661372

##### I heard you should use custom destructors in certain datastructures build on `unique_ptr`?

Nowadays, the use of `new` and `delete` is greatly discouraged in newer C++ versions in favor of `unique_ptr` and `share_ptr`. Unfortunately, `unique_ptr` and `share_ptr` have some drawbacks when used in datastructures with a recursive structure. For example, a node in a linked list contains a pointer to another node. Another example, a node in a binary tree contains two pointers to a "left" node and "right" node. If these pointers are implemented by smart pointers, there is a risk of running into a stack overflow when a recursive datastructure with a large amount of values is destructed. This is caused by the fact that a smart pointer has to call a destructor when destructed, which will thus be called recursively in datastructures with a recursive structure. This can be observed in [1], where they encountered this issue with a tree of only 29000 integers. By implementing a custom destructor, we can use an iterative method and thus omit the danger of a stack overflow.

Currently, the focus lies more on making stuff work in regular use cases than edge case. This is the same reason we currently use recursion here and there instead of stacks or while loops. Perhaps this will be addressed in the future.

[1] Ivaylo Donchev and Emilia Todorova, "Implementation of Binary Search Trees Via Smart Pointers" International Journal of Advanced Computer Science and Applications(IJACSA), 6(3), 2015. http://dx.doi.org/10.14569/IJACSA.2015.060309
