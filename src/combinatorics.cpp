// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "simple-functions.hpp"

#include <cassert>

int permutation(int n)
{
    return simple_functions::factorial(n);
}

int variation(int n, int p)
{
    return (simple_functions::factorial(n) / simple_functions::factorial(n - p));
}

int variation_repetition(int n, int p)
{
    if (p == 0)
    {
        return 0;
    }

    int result = n;
    for (int i = 0; i < (p - 1); i++)
    {
        result *= n;
    }

    return result;
}

int combination(int n, int p)
{
    return simple_functions::binomial_coefficient(n, p);
}

int combination_repetition(int n, int p)
{
    return simple_functions::binomial_coefficient((n - 1 + p), (n - 1));
}