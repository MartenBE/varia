// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "simple-functions.hpp"

#include <cassert>
#include <cmath>

int simple_functions::factorial(int value)
{
    assert(value >= 0);

    if (value == 0)
    {
        return 1;
    }

    return (value * factorial(value - 1));
}

int simple_functions::binomial_coefficient(int n, int k)
{
    return (factorial(n) / (factorial(k) * factorial(n - k)));
}

std::vector<int> simple_functions::sieve_eratosthenes(int max)
{
    std::vector<int> primes;

    std::vector<bool> is_prime((max + 1), true);

    for (std::ptrdiff_t i = 2; i < std::ssize(is_prime); i++)
    {
        if (is_prime[i])
        {
            primes.push_back(i);

            for (std::ptrdiff_t j = (2 * i); j < std::ssize(is_prime); j += i)
            {
                is_prime[j] = false;
            }
        }
    }

    return primes;
}

int simple_functions::euclidean_algorithm(int a, int b)
{
    assert(a >= b);

    if (b == 0)
    {
        return a;
    }

    //  a = q*b + r, and 0 <= r < b

    int r = (a % b);

    return euclidean_algorithm(b, r);
}

double simple_functions::sigmoid(double x)
{
    return (1.0 / (1.0 + std::exp(-x)));
}

int simple_functions::amount_digits(long long int number)
{
    if (number == 0)
    {
        return 1;
    }

    return (std::floor(std::log10(number)) + 1);
}