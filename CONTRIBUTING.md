<!--
Copyright (C) 2020 Martijn Saelens

SPDX-License-Identifier: MIT
-->

When you create a pull request, please follow the following guidelines:

* Every contribution must be covered by thorough unit testing. This is a fundamental corner stone of this repository. Please check the coverage and keep it as high as possible.
* Format your code using the given [clang-format file](./.clang-format).
* Use up-to-date modern C++ code. The latest standard is accepted and even strongly encouraged.
* The code must be [clean code](https://www.pearson.com/store/p/clean-code-a-handbook-of-agile-software-craftsmanship/P100001776638/9780132350884) readable for the average modern C++ programmer.

Code conventions:

* All variable and class names should be in [lower_case_with_underscores_style](https://en.wikipedia.org/wiki/Snake_case).
* Functions returning a bool start with the `is_` prefix. E.g. `is_vertex_present()`. (`empty()` is a notable exception to gain compatibility with the C++ Standard Library data structures, see [std::map<..., ...>::empty()](https://en.cppreference.com/w/cpp/container/map/empty) for example).
* Member variable names are prefixed with the `m_` prefix. Inner classes do not need this prefix. Another exception is `struct` classes, as these are mostly used as a data container which use public data members.
* Getters omit the `get_` prefix. E.g. `value()`.
* Setters start with the `set_` prefix. E.g. `set_value()`.
* Classes must have the following order of scope specifiers:

    ```
    class test
    {
    public:
        // ...

    private:
        // ...

    }
    ```

    If private inner classes are present, an additional private scope can be added at the beginning for forward declarations:

    ```
    class test
    {
    private:
        class inner_test;

    public:
        // ...

    private:
        class inner_test
        {
            // ...
        };
        // ...

    }    
    ```

Licensing:

* Add your name and year to the copyright notice. The year in the copyright notice is the year of initial publication of the file or your first contribution to this file and is not updated by your subsequent contributions. In other words, the copyright notices show who has contributed to the file and the year of their first contribution to this file. E.g. in this example the following has happened: 
    * Martijn has created the file and had modified the file in 2020, 2021, and 2022
    * John Doe has made a contribution in 2021, 2022, 2024, and 2030
    * Some One has made a contribution in 2022, 2023, 2024, 2025, and 2030

    ```
    // Copyright (C) 2020 Martijn Saelens
    // Copyright (C) 2021 John Doe
    // Copyright (C) 2022 Some One
    //
    // SPDX-License-Identifier: MIT

    ```

    * Note: the notice uses a capital C for the copyright sign `(C)`. This is because the [REUSE software](https://reuse.software/) also uses this notation.

Extra help:

* Good and up to date C++ tutorials: https://www.learncpp.com/
* The C++ Core Guidelines: https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines
* The C++ Standard Library reference: https://en.cppreference.com
* Information about implementing iterators: https://www.cs.odu.edu/~zeil/cs361/latest/Public/iterators/
