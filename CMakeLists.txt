# Copyright (C) 2020 Martijn Saelens
#
# SPDX-License-Identifier: MIT

cmake_minimum_required (VERSION 3.14) 

project (Varia)

set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_STANDARD 23)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

message("-- Using C++ ${CMAKE_CXX_STANDARD}")
message("-- Using ${CMAKE_CXX_COMPILER_ID}")
message("-- Using build type ${CMAKE_BUILD_TYPE}")

include(FetchContent)
set(FETCHCONTENT_QUIET FALSE) # Necessary fot GIT_PROGRESS

add_subdirectory(docs)
add_subdirectory(src)
add_subdirectory(tests)
