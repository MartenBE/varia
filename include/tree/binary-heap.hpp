// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef BINARY_HEAP_HPP
#define BINARY_HEAP_HPP

#include <cassert>
#include <functional>
#include <vector>

template <class T>
class binary_heap
{
public:
    void push(const T& element)
    {
        data.push_back(element);
        bottom_up_reheapify(data.begin(), data.end());
    }

    void pop()
    {
        assert(!empty());

        data[0] = std::move(data[std::ssize(data) - 1]);
        data.resize(std::ssize(data) - 1);
        top_down_reheapify(data.begin(), data.end(), data.begin());
    }

    const T& top() const
    {
        assert(!empty());

        return data[0];
    }

    std::ptrdiff_t size() const
    {
        return std::ssize(data);
    }

    bool empty() const
    {
        return data.empty();
    }

    template <class random_access_iterator>
    static void heapify(random_access_iterator begin, random_access_iterator end)
    {
        std::ptrdiff_t amount_non_leaves = (std::distance(begin, end) / 2);

        for (auto root = (begin + amount_non_leaves); root >= begin; root--)
        {
            top_down_reheapify(begin, end, root);
        }
    }

    template <class random_access_iterator>
    static void
    top_down_reheapify(random_access_iterator begin, random_access_iterator end, random_access_iterator root)
    {
        auto left = left_child(begin, root);
        auto right = right_child(begin, root);
        auto greatest_element = root;

        if ((left < end) && (*left > *greatest_element))
        {
            greatest_element = left;
        }

        if ((right < end) && (*right > *greatest_element))
        {
            greatest_element = right;
        }

        if (greatest_element != root)
        {
            std::iter_swap(root, greatest_element);
            top_down_reheapify(begin, end, greatest_element);
        }
    }

private:
    template <class random_access_iterator>
    static void bottom_up_reheapify(random_access_iterator begin, random_access_iterator end)
    {
        auto new_data = (end - 1);
        auto parent_new_data = parent_index(begin, new_data);
        while ((new_data > begin) && (parent_new_data >= begin) && (*new_data > *parent_new_data))
        {
            std::iter_swap(new_data, parent_new_data);

            new_data = parent_new_data;
            parent_new_data = parent_index(begin, parent_new_data);
        }
    }

    template <class random_access_iterator>
    static random_access_iterator parent_index(random_access_iterator begin, random_access_iterator i)
    {
        i--;
        i = begin + std::distance(begin, i) / 2;

        return i;
    }

    template <class random_access_iterator>
    static random_access_iterator left_child(random_access_iterator begin, random_access_iterator i)
    {
        i += (std::distance(begin, i) + 1);

        return i;
    }

    template <class random_access_iterator>
    static random_access_iterator right_child(random_access_iterator begin, random_access_iterator i)
    {
        i += (std::distance(begin, i) + 2);

        return i;
    }

    std::vector<T> data;
};

#endif