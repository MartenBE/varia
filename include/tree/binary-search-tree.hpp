// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef BINARY_SEARCH_TREE_H
#define BINARY_SEARCH_TREE_H

#include <cassert>
#include <functional>
#include <initializer_list>
#include <memory>
#include <utility>

template <class K, class V>
class binary_search_tree
{
private:
    struct node;
    class iterator;

public:
    binary_search_tree() = default;

    binary_search_tree(std::initializer_list<std::pair<K, V>> il)
    {
        for (const auto& i : il)
        {
            insert(i);
        }
    }

    binary_search_tree(const binary_search_tree& other) : m_size{other.m_size}
    {
        deep_copy_tree(m_root, other.m_root);
    }

    binary_search_tree& operator=(const binary_search_tree& other)
    {
        binary_search_tree temp{other};
        swap(*this, temp);

        return *this;
    }

    binary_search_tree(binary_search_tree&& other) noexcept : m_root{std::move(other.m_root)}, m_size{other.m_size}
    {
        other.m_size = 0;
    }

    binary_search_tree& operator=(binary_search_tree&& other) noexcept
    {
        binary_search_tree temp{std::move(other)};
        swap(*this, temp);

        return *this;
    }

    friend void swap(binary_search_tree& lhs, binary_search_tree& rhs) noexcept
    {
        using std::swap;
        swap(lhs.m_root, rhs.m_root);
        swap(lhs.m_size, rhs.m_size);
    }

    bool empty() const
    {
        return (m_root == nullptr);
    }

    std::ptrdiff_t size() const
    {
        return m_size;
    }

    void clear()
    {
        m_root.reset();
        m_size = 0;
    }

    iterator insert(const std::pair<K, V>& data)
    {
        std::unique_ptr<node> new_node_owning_ptr = std::make_unique<node>();
        new_node_owning_ptr->m_data = data;
        iterator new_node_iterator{new_node_owning_ptr.get()};

        auto [found_node, parent_node] = tree_search(m_root.get(), data.first);
        assert(!found_node);

        new_node_owning_ptr->m_parent = parent_node;

        if (!parent_node)
        {
            m_root = std::move(new_node_owning_ptr);
        }
        else
        {
            direction next_direction;
            if (data.first < parent_node->m_data.first)
            {
                next_direction = LEFT;
            }
            else
            {
                next_direction = RIGHT;
            }

            parent_node->m_children[next_direction] = std::move(new_node_owning_ptr);
        }

        m_size++;
        return new_node_iterator;
    }

    void erase(const iterator& it)
    {
        erase(it.m_iterator_ptr);
    }

    V& operator[](const K& key)
    {
        auto [found_node, parent_node] = tree_search(m_root.get(), key);

        if (found_node)
        {
            return found_node->m_data.second;
        }
        else
        {
            iterator result = insert({key, V{}});
            return result->second;
        }
    }

    const V& at(const K& key) const
    {
        auto [found_node, parent_node] = tree_search(m_root.get(), key);
        assert(found_node);

        return found_node->m_data.second;
    }

    bool contains(const K& key) const
    {
        auto [found_node, parent_node] = tree_search(m_root.get(), key);
        return (found_node != nullptr);
    }

    friend bool operator==(const binary_search_tree& lhs, const binary_search_tree& rhs)
    {
        if (lhs.size() != rhs.size())
        {
            return false;
        }

        return are_trees_equal(lhs.m_root.get(), rhs.m_root.get());
    }

    friend bool operator!=(const binary_search_tree& lhs, const binary_search_tree& rhs)
    {
        return !(lhs == rhs);
    }

    iterator find(const K& key)
    {
        auto [found_node, parent_node] = tree_search(m_root.get(), key);
        return iterator{found_node};
    }

    iterator begin()
    {
        return iterator{tree_minimum(m_root.get())};
    }

    iterator end()
    {
        return iterator{nullptr};
    }

private:
    static std::pair<node*, node*> tree_search(node* root_node, const K& key)
    {
        node* current_node = root_node;
        node* parent_node = nullptr;
        while (current_node && (key != current_node->m_data.first))
        {
            parent_node = current_node;

            direction next_direction;
            if (key < current_node->m_data.first)
            {
                next_direction = LEFT;
            }
            else
            {
                next_direction = RIGHT;
            }

            current_node = current_node->m_children[next_direction].get();
        }

        return {current_node, parent_node};
    }

    static node* tree_minimum(node* root_node)
    {
        assert(root_node);

        node* current_node = root_node;

        while (current_node->m_children[LEFT])
        {
            current_node = current_node->m_children[LEFT].get();
        }

        return current_node;
    }

    static node* tree_successor(node* root_node)
    {
        assert(root_node);

        if (root_node->m_children[RIGHT])
        {
            return tree_minimum(root_node->m_children[RIGHT].get());
        }

        node* current_node = root_node;
        node* parent_node = current_node->m_parent;

        while (parent_node && (parent_node->m_children[RIGHT].get() == current_node))
        {
            current_node = parent_node;
            parent_node = current_node->m_parent;
        }

        return parent_node;
    }

    static bool are_trees_equal(node* lhs_root, node* rhs_root)
    {
        if (!lhs_root && !rhs_root)
        {
            return true;
        }

        if ((lhs_root && !rhs_root) || (!lhs_root && rhs_root))
        {
            return false;
        }

        if (lhs_root->m_data != rhs_root->m_data)
        {
            return false;
        }

        for (const auto& d : {LEFT, RIGHT})
        {
            if (!are_trees_equal(lhs_root->m_children[d].get(), rhs_root->m_children[d].get()))
            {
                return false;
            }
        }

        return true;
    }

    std::unique_ptr<node>* get_node_owning_ptr(node* current_node)
    {
        assert(current_node);

        node* parent_node = current_node->m_parent;

        if (!parent_node)
        {
            return &m_root;
        }

        direction d;
        if (current_node == parent_node->m_children[LEFT].get())
        {
            d = LEFT;
        }
        else
        {
            d = RIGHT;
        }

        return &(parent_node->m_children[d]);
    }

    void erase(node* node_to_delete)
    {
        assert(node_to_delete);

        if (node_to_delete->m_children[LEFT] && node_to_delete->m_children[RIGHT])
        {
            node* min_node = tree_minimum(node_to_delete->m_children[RIGHT].get());
            node_to_delete->m_data = min_node->m_data;
            erase(min_node); // Successor doesn't have a left child
        }
        else
        {
            std::unique_ptr<node>& node_to_delete_owning_ptr = *get_node_owning_ptr(node_to_delete);

            if (!node_to_delete->m_children[LEFT] && !node_to_delete->m_children[RIGHT])
            {
                node_to_delete_owning_ptr.reset();
            }
            else if (!node_to_delete->m_children[LEFT])
            {
                node_to_delete->m_children[RIGHT]->m_parent = node_to_delete->m_parent;
                node_to_delete_owning_ptr = std::move(node_to_delete->m_children[RIGHT]);
            }
            else if (!node_to_delete->m_children[RIGHT])
            {
                node_to_delete->m_children[LEFT]->m_parent = node_to_delete->m_parent;
                node_to_delete_owning_ptr = std::move(node_to_delete->m_children[LEFT]);
            }

            m_size--;
        }
    }

    void deep_copy_tree(std::unique_ptr<node>& target_root_node, const std::unique_ptr<node>& source_root_node)
    {
        if (!source_root_node)
        {
            target_root_node.reset();
            return;
        }

        target_root_node = std::make_unique<node>();
        target_root_node->m_data = source_root_node->m_data;

        for (const auto d : {LEFT, RIGHT})
        {
            deep_copy_tree(target_root_node->m_children[d], source_root_node->m_children[d]);

            if (target_root_node->m_children[d])
            {
                target_root_node->m_children[d]->m_parent = target_root_node.get();
            }
        }
    }

    enum direction
    {
        LEFT = 0,
        RIGHT = 1
    };

    struct node
    {
        std::pair<K, V> m_data;
        std::array<std::unique_ptr<node>, 2> m_children;
        node* m_parent = nullptr;
    };

    class iterator
    {
        friend class binary_search_tree;

    public:
        using value_type = std::pair<const K&, V&>;
        using pointer = value_type*;
        using reference = value_type&;
        using difference_type = std::ptrdiff_t;
        using iterator_category = std::input_iterator_tag;

        iterator& operator++()
        {
            m_iterator_ptr = tree_successor(m_iterator_ptr);

            return *this;
        }

        iterator operator++(int)
        {
            iterator temp{*this};
            ++(*this);

            return temp;
        }

        friend bool operator==(const iterator& lhs, const iterator& rhs)
        {
            return (lhs.m_iterator_ptr == rhs.m_iterator_ptr);
        }

        friend bool operator!=(const iterator& lhs, const iterator& rhs)
        {
            return !(lhs == rhs);
        }

        reference operator*()
        {
            update_dummy();
            return *dummy;
        }

        pointer operator->()
        {
            update_dummy();
            return dummy.get();
        }

    private:
        iterator(node* iterator_ptr) : m_iterator_ptr{iterator_ptr}
        {
        }

        void update_dummy()
        {
            assert(m_iterator_ptr);
            dummy = std::make_unique<value_type>(m_iterator_ptr->m_data.first, m_iterator_ptr->m_data.second);
        }

        node* m_iterator_ptr = nullptr;
        std::unique_ptr<value_type> dummy;
    };

    std::unique_ptr<node> m_root = nullptr;
    std::ptrdiff_t m_size = 0;
};

#endif