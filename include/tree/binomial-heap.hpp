// Copyright (C) 2021 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef BINOMIAL_HEAP_HPP
#define BINOMIAL_HEAP_HPP

#include "exceptions.hpp"

#include <algorithm>
#include <cmath>
#include <memory>

template <class T>
class binomial_heap
{
private:
    class binomial_tree;

public:
    void push(const T& element)
    {
        binomial_heap new_heap;
        new_heap.m_trees.push_back(binomial_tree{element});

        merge(std::move(new_heap));
    }

    void pop()
    {
        assert(!empty());

        std::ptrdiff_t max_index = top_index();
        binomial_tree max_tree = std::move(m_trees[max_index]);
        std::vector<binomial_tree> subtrees = max_tree.pop();

        binomial_heap heap_with_subtrees;
        heap_with_subtrees.m_trees.resize(subtrees.size());

        for (auto&& subtree : subtrees)
        {
            heap_with_subtrees.m_trees[subtree.degree()] = std::move(subtree);
        }

        this->merge(std::move(heap_with_subtrees));
    }

    const T& top() const
    {
        assert(!empty());

        std::ptrdiff_t max_index = top_index();

        return m_trees[max_index].data();
    }

    std::ptrdiff_t size() const
    {
        std::ptrdiff_t sum = 0;

        for (const auto& tree : m_trees)
        {
            sum += tree.size();
        }

        return sum;
    }

    bool empty() const
    {
        for (const auto& tree : m_trees)
        {
            if (!tree.empty())
            {
                return false;
            }
        }

        return true;
    }

private:
    void merge(binomial_heap&& other)
    {
        std::ptrdiff_t max_amount_trees = std::max(std::ssize(m_trees), std::ssize(other.m_trees));
        if (std::ssize(m_trees) < max_amount_trees)
        {
            m_trees.resize(max_amount_trees);
        }

        binomial_tree carry;
        std::ptrdiff_t min_amount_trees = std::min(std::ssize(m_trees), std::ssize(other.m_trees));
        std::ptrdiff_t i = 0;
        while (i < min_amount_trees)
        {
            bool is_tree_empty = m_trees[i].empty();
            bool is_other_tree_empty = other.m_trees[i].empty();

            if (carry.empty())
            {
                if (is_tree_empty && is_other_tree_empty)
                {
                    // Do nothing: nothing to merge
                }
                else if (is_tree_empty && !is_other_tree_empty)
                {
                    m_trees[i] = std::move(other.m_trees[i]);
                }
                else if (!is_tree_empty && is_other_tree_empty)
                {
                    // Do nothing: m_trees[i] is already in place
                }
                else // !is_tree_empty && !is_other_tree_empty
                {
                    m_trees[i].merge(std::move(other.m_trees[i]));
                    carry = std::move(m_trees[i]);
                }
            }
            else
            {
                if (is_tree_empty && is_other_tree_empty)
                {
                    m_trees[i] = std::move(carry);
                }
                else if (is_tree_empty && !is_other_tree_empty)
                {
                    carry.merge(std::move(other.m_trees[i]));
                }
                else if (!is_tree_empty && is_other_tree_empty)
                {
                    carry.merge(std::move(m_trees[i]));
                }
                else // !is_tree_empty && !is_other_tree_empty
                {
                    carry.merge(std::move(other.m_trees[i]));
                }
            }

            i++;
        }

        while (i < std::ssize(m_trees) && !carry.empty())
        {
            if (m_trees[i].empty())
            {
                m_trees[i] = std::move(carry);
            }
            else
            {
                carry.merge(std::move(m_trees[i]));
            }

            i++;
        }

        if (!carry.empty())
        {
            m_trees.push_back(std::move(carry));
        }
    }

    std::ptrdiff_t top_index() const
    {
        std::ptrdiff_t max_index = -1;
        for (std::ptrdiff_t i = 0; i < std::ssize(m_trees); i++)
        {
            if (!m_trees[i].empty())
            {
                if ((max_index == -1) || (m_trees[i].data() > m_trees[max_index].data()))
                {
                    max_index = i;
                }
            }
        }

        return max_index;
    }

    class binomial_tree
    {
    private:
        static constexpr std::ptrdiff_t NO_DEGREE = -1;
        class node;

    public:
        binomial_tree() = default;

        binomial_tree(const T& data) : m_root{std::make_unique<node>(data)}, m_degree{0}
        {
        }

        binomial_tree(const binomial_tree& other) = delete;
        binomial_tree& operator=(const binomial_tree& other) = delete;

        binomial_tree(binomial_tree&& other) : m_root{std::move(other.m_root)}, m_degree{other.m_degree}
        {
            other.clear();
        }

        binomial_tree& operator=(binomial_tree&& other)
        {
            binomial_tree temp{std::move(other)};
            swap(*this, temp);

            return *this;
        }

        friend void swap(binomial_tree& lhs, binomial_tree& rhs)
        {
            using std::swap;
            swap(lhs.m_root, rhs.m_root);
            swap(lhs.m_degree, rhs.m_degree);
        }

        const T& data() const
        {
            assert(m_root);

            return m_root->m_data;
        }

        std::ptrdiff_t degree() const
        {
            assert(!empty());

            return m_degree;
        }

        std::ptrdiff_t size() const
        {
            if (empty())
            {
                return 0;
            }

            return static_cast<std::ptrdiff_t>(std::pow(2, m_degree));
        }

        bool empty() const
        {
            return (m_root == nullptr);
        }

        void merge(binomial_tree&& other)
        {
            assert(!empty());
            assert(!other.empty());
            assert(size() == other.size());

            binomial_tree* primary = this;
            binomial_tree* secondary = &other;

            if (data() < other.data())
            {
                std::swap(primary, secondary);
            }

            primary->m_root->m_children.push_back(std::move(*secondary));
            (primary->m_degree)++;

            *this = std::move(*primary);
        }

        std::vector<binomial_tree> pop()
        {
            assert(!empty());

            std::vector<binomial_tree> subtrees = std::move(m_root->m_children);
            clear();

            return subtrees;
        }

        void clear()
        {
            m_root.reset();
            m_degree = NO_DEGREE;
        }

    private:
        class node
        {
        public:
            node(const T& data) : m_data{data}
            {
            }

            const T m_data;
            std::vector<binomial_tree> m_children;
        };

        std::unique_ptr<node> m_root;
        std::ptrdiff_t m_degree = NO_DEGREE;
    };

    std::vector<binomial_tree> m_trees;
};

#endif