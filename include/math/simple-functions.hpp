// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef SIMPLE_FUNCTIONS_HPP
#define SIMPLE_FUNCTIONS_HPP

#include <vector>

class simple_functions
{
public:
    static int factorial(int value);
    static int binomial_coefficient(int n, int k);
    static std::vector<int> sieve_eratosthenes(int max);
    static int euclidean_algorithm(int a, int b);
    static double sigmoid(double x);
    static int amount_digits(long long int number);
};

#endif