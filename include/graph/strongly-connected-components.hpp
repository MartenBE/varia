// Copyright (C) 2021 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef STRONGLY_CONNECTED_COMPONENTS_HPP
#define STRONGLY_CONNECTED_COMPONENTS_HPP

#include "exceptions.hpp"
#include "graph.hpp"
#include "topological-sort.hpp"

template <class V, class E>
class strongly_connected_components
{
public:
    static std::set<std::set<std::ptrdiff_t>> kosaraju(const graph<V, E>& original_graph)
    {
        std::ptrdiff_t start_vertex_id = *(original_graph.vertex_ids().begin());
        std::vector<std::ptrdiff_t> postorder_vertex_ids =
                visit_nodes<V, E>::post_order(original_graph, start_vertex_id);
        std::reverse(postorder_vertex_ids.begin(), postorder_vertex_ids.end());

        graph<V, E> reverse_graph{original_graph};
        reverse_graph.transpose();

        // TODO let visit_node.hpp functions do this ...

        std::unordered_map<std::ptrdiff_t, bool> is_vertex_visited;

        for (const auto& vertex_id : postorder_vertex_ids)
        {
            is_vertex_visited[vertex_id] = false;
        }

        std::set<std::set<std::ptrdiff_t>> components;

        for (const auto& vertex_id : postorder_vertex_ids)
        {
            if (!is_vertex_visited[vertex_id])
            {
                std::set<std::ptrdiff_t> vertex_ids;
                is_vertex_visited[vertex_id] = true;
                depth_first_search_recursive_visit(
                        reverse_graph,
                        vertex_id,
                        is_vertex_visited,
                        [](const auto&) {},
                        [&vertex_ids](const auto& vertex_id) { vertex_ids.insert(vertex_id); });

                components.insert(vertex_ids);
            }
        }

        return components;
    }

    // static std::set<std::set<int>> tarjan(const graph<V, E>& original_graph)
    // {
    //     throw not_implemented_exception();
    // }

private:
    static void depth_first_search_recursive_visit(const graph<V, E>& original_graph,
                                                   std::ptrdiff_t vertex_id,
                                                   std::unordered_map<std::ptrdiff_t, bool>& is_vertex_visited,
                                                   std::function<void(std::ptrdiff_t vertex_id)> prefix_function,
                                                   std::function<void(std::ptrdiff_t vertex_id)> postfix_function)
    {
        prefix_function(vertex_id);

        for (const auto& edge_id : original_graph.edge_ids(vertex_id))
        {
            std::ptrdiff_t neighbor_vertex_id = original_graph.edge_target_vertex_id(edge_id);

            if (!is_vertex_visited[neighbor_vertex_id])
            {
                is_vertex_visited[neighbor_vertex_id] = true;

                depth_first_search_recursive_visit(
                        original_graph, neighbor_vertex_id, is_vertex_visited, prefix_function, postfix_function);
            }
        }

        postfix_function(vertex_id);
    }
};

#endif