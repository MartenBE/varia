// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef PATH_SEARCH_HPP
#define PATH_SEARCH_HPP

#include "graph.hpp"

#include <limits>
#include <optional>
#include <stack>
#include <unordered_map>
#include <vector>

template <class V, class E>
class path_search
{
public:
    using path = std::vector<std::ptrdiff_t>;
    static constexpr std::ptrdiff_t invalid_vertex_id = std::numeric_limits<std::ptrdiff_t>::min();

    static std::optional<path> depth_first_search_recursive(const graph<V, E>& original_graph,
                                                            std::ptrdiff_t start_vertex_id,
                                                            std::ptrdiff_t target_vertex_id)
    {
        assert(original_graph.is_vertex_present(start_vertex_id));
        assert(original_graph.is_vertex_present(target_vertex_id));

        if (start_vertex_id == target_vertex_id)
        {
            return std::optional<path>{std::in_place, {start_vertex_id}};
        }

        std::unordered_map<std::ptrdiff_t, bool> is_vertex_visited;
        std::unordered_map<std::ptrdiff_t, std::ptrdiff_t> vertex_parent_id;

        for (std::ptrdiff_t vertex_id : original_graph.vertex_ids())
        {
            is_vertex_visited[vertex_id] = false;
            vertex_parent_id[vertex_id] = invalid_vertex_id;
        }

        bool has_path = depth_first_search_recursive_visit(
                original_graph, start_vertex_id, target_vertex_id, is_vertex_visited, vertex_parent_id);

        if (has_path)
        {
            return std::optional<path>{construct_path(start_vertex_id, target_vertex_id, vertex_parent_id)};
        }

        return std::nullopt;
    }

    static std::optional<path> depth_first_search_iterative(const graph<V, E>& original_graph,
                                                            std::ptrdiff_t start_vertex_id,
                                                            std::ptrdiff_t target_vertex_id)
    {
        assert(original_graph.is_vertex_present(start_vertex_id));
        assert(original_graph.is_vertex_present(target_vertex_id));

        if (start_vertex_id == target_vertex_id)
        {
            return std::optional<path>{std::in_place, {start_vertex_id}};
        }

        std::unordered_map<std::ptrdiff_t, bool> is_vertex_visited;
        std::unordered_map<std::ptrdiff_t, std::ptrdiff_t> vertex_parent_id;

        for (std::ptrdiff_t vertex_id : original_graph.vertex_ids())
        {
            is_vertex_visited[vertex_id] = false;
            vertex_parent_id[vertex_id] = invalid_vertex_id;
        }

        std::queue<std::ptrdiff_t> vertex_ids_to_process;
        vertex_ids_to_process.push(start_vertex_id);
        is_vertex_visited[start_vertex_id] = true;

        while (!vertex_ids_to_process.empty())
        {
            std::ptrdiff_t vertex_id = vertex_ids_to_process.front();
            vertex_ids_to_process.pop();

            for (const auto& edge_id : original_graph.edge_ids(vertex_id))
            {
                std::ptrdiff_t neighbor_vertex_id = original_graph.edge_target_vertex_id(edge_id);

                if (!is_vertex_visited[neighbor_vertex_id])
                {
                    vertex_ids_to_process.push(neighbor_vertex_id);
                    is_vertex_visited[neighbor_vertex_id] = true;
                    vertex_parent_id[neighbor_vertex_id] = vertex_id;

                    if (neighbor_vertex_id == target_vertex_id)
                    {
                        return std::optional<path>{construct_path(start_vertex_id, target_vertex_id, vertex_parent_id)};
                    }
                }
            }
        }

        return std::nullopt;
    }

    static std::optional<path> breadth_first_search(const graph<V, E>& original_graph,
                                                    std::ptrdiff_t start_vertex_id,
                                                    std::ptrdiff_t target_vertex_id)
    {
        assert(original_graph.is_vertex_present(start_vertex_id));
        assert(original_graph.is_vertex_present(target_vertex_id));

        if (start_vertex_id == target_vertex_id)
        {
            return std::optional<path>{std::in_place, {start_vertex_id}};
        }

        std::unordered_map<std::ptrdiff_t, bool> is_vertex_visited;
        std::unordered_map<std::ptrdiff_t, std::ptrdiff_t> vertex_parent_id;

        for (std::ptrdiff_t vertex_id : original_graph.vertex_ids())
        {
            is_vertex_visited[vertex_id] = false;
            vertex_parent_id[vertex_id] = invalid_vertex_id;
        }

        std::queue<std::ptrdiff_t> vertex_ids_to_process;
        vertex_ids_to_process.push(start_vertex_id);
        is_vertex_visited[start_vertex_id] = true;

        while (!vertex_ids_to_process.empty())
        {
            std::ptrdiff_t vertex_id = vertex_ids_to_process.front();
            vertex_ids_to_process.pop();

            for (const auto& edge_id : original_graph.edge_ids(vertex_id))
            {
                std::ptrdiff_t neighbor_vertex_id = original_graph.edge_target_vertex_id(edge_id);

                if (!is_vertex_visited[neighbor_vertex_id])
                {
                    vertex_ids_to_process.push(neighbor_vertex_id);
                    is_vertex_visited[neighbor_vertex_id] = true;
                    vertex_parent_id[neighbor_vertex_id] = vertex_id;

                    if (neighbor_vertex_id == target_vertex_id)
                    {
                        return std::optional<path>{construct_path(start_vertex_id, target_vertex_id, vertex_parent_id)};
                    }
                }
            }
        }

        return std::nullopt;
    }

private:
    static bool depth_first_search_recursive_visit(const graph<V, E>& original_graph,
                                                   std::ptrdiff_t vertex_id,
                                                   std::ptrdiff_t target_vertex_id,
                                                   std::unordered_map<std::ptrdiff_t, bool>& is_vertex_visited,
                                                   std::unordered_map<std::ptrdiff_t, std::ptrdiff_t>& vertex_parent_id)
    {
        if (vertex_id == target_vertex_id)
        {
            return true;
        }

        for (const auto& edge_id : original_graph.edge_ids(vertex_id))
        {
            std::ptrdiff_t neighbor_vertex_id = original_graph.edge_target_vertex_id(edge_id);

            if (!is_vertex_visited[neighbor_vertex_id])
            {
                is_vertex_visited[neighbor_vertex_id] = true;
                vertex_parent_id[neighbor_vertex_id] = vertex_id;

                bool has_path = depth_first_search_recursive_visit(
                        original_graph, neighbor_vertex_id, target_vertex_id, is_vertex_visited, vertex_parent_id);

                if (has_path)
                {
                    return true;
                }
            }
        }

        return false;
    }

    static path construct_path(std::ptrdiff_t start_vertex_id,
                               std::ptrdiff_t target_vertex_id,
                               const std::unordered_map<std::ptrdiff_t, std::ptrdiff_t>& vertex_parent_id)
    {
        path path_vertex_ids;

        std::ptrdiff_t vertex_id = target_vertex_id;
        while (vertex_id != start_vertex_id)
        {
            path_vertex_ids.push_back(vertex_id);
            vertex_id = vertex_parent_id.at(vertex_id);
        }

        path_vertex_ids.push_back(start_vertex_id);

        std::reverse(path_vertex_ids.begin(), path_vertex_ids.end());

        return path_vertex_ids;
    }
};

#endif