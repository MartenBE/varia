// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef FLOYD_WARSHALL_HPP
#define FLOYD_WARSHALL_HPP

#include "graph.hpp"
#include "matrix.hpp"

template <class V, class E>
class floyd_warshall
{
private:
    constexpr static E infinity = std::numeric_limits<E>::max();

public:
    floyd_warshall(const graph<V, E>& original_graph)
    : m_original_graph{&original_graph}, m_distances{original_graph.amount_vertices(), original_graph.amount_vertices()}
    {
        const std::ptrdiff_t amount_vertices = m_original_graph->amount_vertices();

        for (std::ptrdiff_t i = 0; i < amount_vertices; i++)
        {
            for (std::ptrdiff_t j = 0; j < amount_vertices; j++)
            {
                if (i == j)
                {
                    m_distances(i, j) = E{0};
                }
                else
                {
                    m_distances(i, j) = infinity;
                }
            }
        }

        for (const auto& vertex_id : m_original_graph->vertex_ids())
        {
            for (const auto& edge_id : m_original_graph->edge_ids(vertex_id))
            {
                m_distances(vertex_id, m_original_graph->edge_target_vertex_id(edge_id)) =
                        m_original_graph->edge_data(edge_id);
            }
        }

        for (std::ptrdiff_t k = 0; k < amount_vertices; k++)
        {
            matrix<int> distances_with_vertex_k{m_distances};

            for (std::ptrdiff_t i = 0; i < amount_vertices; i++)
            {
                for (std::ptrdiff_t j = 0; j < amount_vertices; j++)
                {
                    if ((m_distances(i, k) != infinity) && (m_distances(k, j) != infinity)) // Overflow prevention
                    {
                        distances_with_vertex_k(i, j) =
                                std::min(m_distances(i, j), (m_distances(i, k) + m_distances(k, j)));
                    }
                }
            }

            m_distances = std::move(distances_with_vertex_k);
        }
    }

    const matrix<E>& distances() const
    {
        return m_distances;
    }

private:
    const graph<V, E>* m_original_graph;
    matrix<E> m_distances;
};

#endif