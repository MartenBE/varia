// Copyright (C) 2021 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef VIST_NODES_HPP
#define VIST_NODES_HPP

#include "graph.hpp"

#include <optional>
#include <unordered_map>

template <class V, class E>
class visit_nodes
{
public:
    static inline std::function<void(std::ptrdiff_t vertex_id)> noop = [](std::ptrdiff_t) {
    };

    static std::vector<std::ptrdiff_t> pre_order(const graph<V, E>& original_graph, std::ptrdiff_t start_vertex_id)
    {
        std::vector<std::ptrdiff_t> preorder_vertex_ids;
        auto store_preorder_vertex_ids = [&preorder_vertex_ids](std::ptrdiff_t vertex_id)
        {
            preorder_vertex_ids.push_back(vertex_id);
        };

        visit_nodes<std::string, int>::depth_first_search_recursive(
                original_graph, start_vertex_id, store_preorder_vertex_ids, noop);

        return preorder_vertex_ids;
    }

    static std::vector<std::ptrdiff_t> post_order(const graph<V, E>& original_graph, std::ptrdiff_t start_vertex_id)
    {
        std::vector<std::ptrdiff_t> postorder_vertex_ids;
        auto store_postorder_vertex_ids = [&postorder_vertex_ids](std::ptrdiff_t vertex_id)
        {
            postorder_vertex_ids.push_back(vertex_id);
        };

        visit_nodes<std::string, int>::depth_first_search_recursive(
                original_graph, start_vertex_id, noop, store_postorder_vertex_ids);

        return postorder_vertex_ids;
    }

    static void depth_first_search_recursive(const graph<V, E>& original_graph,
                                             std::ptrdiff_t start_vertex_id,
                                             std::function<void(std::ptrdiff_t vertex_id)> prefix_function,
                                             std::function<void(std::ptrdiff_t vertex_id)> postfix_function)
    {
        assert(original_graph.is_vertex_present(start_vertex_id));

        std::unordered_map<std::ptrdiff_t, bool> is_vertex_visited;

        for (std::ptrdiff_t vertex_id : original_graph.vertex_ids())
        {
            is_vertex_visited[vertex_id] = false;
        }

        // This isn't really necessary, but allows for a form of (partial) determinism.
        //
        is_vertex_visited[start_vertex_id] = true;
        depth_first_search_recursive_visit(
                original_graph, start_vertex_id, is_vertex_visited, prefix_function, postfix_function);

        for (std::ptrdiff_t vertex_id : original_graph.vertex_ids())
        {
            if (!is_vertex_visited[vertex_id])
            {
                is_vertex_visited[vertex_id] = true;
                depth_first_search_recursive_visit(
                        original_graph, vertex_id, is_vertex_visited, prefix_function, postfix_function);
            }
        }
    }

private:
    static void depth_first_search_recursive_visit(const graph<V, E>& original_graph,
                                                   std::ptrdiff_t vertex_id,
                                                   std::unordered_map<std::ptrdiff_t, bool>& is_vertex_visited,
                                                   std::function<void(std::ptrdiff_t vertex_id)> prefix_function,
                                                   std::function<void(std::ptrdiff_t vertex_id)> postfix_function)
    {
        prefix_function(vertex_id);

        for (const auto& edge_id : original_graph.edge_ids(vertex_id))
        {
            std::ptrdiff_t neighbor_vertex_id = original_graph.edge_target_vertex_id(edge_id);

            if (!is_vertex_visited[neighbor_vertex_id])
            {
                is_vertex_visited[neighbor_vertex_id] = true;

                depth_first_search_recursive_visit(
                        original_graph, neighbor_vertex_id, is_vertex_visited, prefix_function, postfix_function);
            }
        }

        postfix_function(vertex_id);
    }
};

#endif