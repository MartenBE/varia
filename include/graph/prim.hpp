// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef PRIM_HPP
#define PRIM_HPP

#include "graph.hpp"

#include <algorithm>
#include <cassert>
#include <functional>
#include <queue>
#include <tuple>
#include <unordered_set>
#include <vector>

template <class V, class E>
class prim
{
public:
    prim(const graph<V, E>& original_graph)
    {
        auto vertex_ids = original_graph.vertex_ids();
        int start_vertex_id = *std::min_element(vertex_ids.begin(), vertex_ids.end());

        m_minimum_spanning_tree = original_graph;
        m_minimum_spanning_tree.clear_edges();

        using edge = std::pair<E, int>;
        std::priority_queue<edge, std::vector<edge>, std::greater<edge>> edges_to_add;

        for (const auto& id : original_graph.edge_ids(start_vertex_id))
        {
            edges_to_add.push({original_graph.edge_data(id), id});
        }

        std::set<int> vertices_in_tree;
        vertices_in_tree.insert(start_vertex_id);

        while (!edges_to_add.empty())
        {
            auto [weight, id] = std::move(edges_to_add.top());
            edges_to_add.pop();
            int target_vertex_id = original_graph.edge_target_vertex_id(id);

            if (!vertices_in_tree.contains(target_vertex_id))
            {
                int source_vertex_id = original_graph.edge_source_vertex_id(id);

                m_minimum_spanning_tree.add_edge(source_vertex_id, target_vertex_id, weight);
                vertices_in_tree.insert(target_vertex_id);

                for (const auto& id : original_graph.edge_ids(target_vertex_id))
                {
                    edges_to_add.push({original_graph.edge_data(id), id});
                }
            }
        }

        m_sum_edge_weights = 0;

        for (const auto& id : m_minimum_spanning_tree.edge_ids())
        {
            m_sum_edge_weights += m_minimum_spanning_tree.edge_data(id);
        }
    }

    graph<V, E> minimum_spanning_tree() const
    {
        return m_minimum_spanning_tree;
    }

    int sum_edge_weights() const
    {
        return m_sum_edge_weights;
    }

private:
    graph<V, E> m_minimum_spanning_tree;
    int m_sum_edge_weights;
};

#endif