// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef KRUSKAL_HPP
#define KRUSKAL_HPP

#include "disjoint-sets-forests.hpp"
#include "graph.hpp"

template <class V, class E>
class kruskal
{
public:
    kruskal(const graph<V, E>& original_graph)
    {
        disjoint_sets_forests<std::ptrdiff_t> vertex_ids;
        using edge = std::pair<E, int>;
        std::priority_queue<edge, std::vector<edge>, std::greater<edge>> edges_to_add;

        m_minimum_spanning_tree = original_graph;
        m_minimum_spanning_tree.clear_edges();

        for (const auto& vertex_id : original_graph.vertex_ids())
        {
            vertex_ids.make_set(vertex_id);

            for (const auto& edge_id : original_graph.edge_ids(vertex_id))
            {
                edges_to_add.push({original_graph.edge_data(edge_id), edge_id});
            }
        }

        while (!edges_to_add.empty())
        {
            auto [weight, edge_id] = std::move(edges_to_add.top());
            edges_to_add.pop();

            std::ptrdiff_t source_vertex_id = original_graph.edge_source_vertex_id(edge_id);
            std::ptrdiff_t target_vertex_id = original_graph.edge_target_vertex_id(edge_id);

            if (vertex_ids.find_set(source_vertex_id) != vertex_ids.find_set(target_vertex_id))
            {
                m_minimum_spanning_tree.add_edge(source_vertex_id, target_vertex_id, weight);

                vertex_ids.make_union(source_vertex_id, target_vertex_id);
            }
        }

        m_sum_edge_weights = 0;

        for (const auto& id : m_minimum_spanning_tree.edge_ids())
        {
            m_sum_edge_weights += m_minimum_spanning_tree.edge_data(id);
        }
    }

    graph<V, E> minimum_spanning_tree() const
    {
        return m_minimum_spanning_tree;
    }

    int sum_edge_weights() const
    {
        return m_sum_edge_weights;
    }

private:
    graph<V, E> m_minimum_spanning_tree;
    int m_sum_edge_weights;
};

#endif