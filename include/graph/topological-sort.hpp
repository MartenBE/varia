// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef TOPOLOGICAL_SORT_HPP
#define TOPOLOGICAL_SORT_HPP

#include "graph.hpp"
#include "visit-nodes.hpp"

#include <algorithm>
#include <vector>

template <class V, class E>
class topological_sort
{
public:
    static std::vector<std::ptrdiff_t> depth_first_search(const graph<V, E>& original_graph)
    {
        if (original_graph.empty())
        {
            return std::vector<std::ptrdiff_t>{};
        }

        std::ptrdiff_t start_vertex_id = *(original_graph.vertex_ids().begin());
        std::vector<std::ptrdiff_t> sorted_vertex_ids = visit_nodes<V, E>::post_order(original_graph, start_vertex_id);
        std::reverse(sorted_vertex_ids.begin(), sorted_vertex_ids.end());

        return sorted_vertex_ids;
    }

    static std::vector<std::ptrdiff_t> kahn(const graph<V, E>& original_graph)
    {
        if (original_graph.empty())
        {
            return std::vector<std::ptrdiff_t>{};
        }

        std::unordered_map<std::ptrdiff_t, int> indegrees;

        for (std::ptrdiff_t vertex_id : original_graph.vertex_ids())
        {
            indegrees[vertex_id] = 0;
        }

        for (std::ptrdiff_t edge_id : original_graph.edge_ids())
        {
            std::ptrdiff_t target_vertex_id = original_graph.edge_target_vertex_id(edge_id);
            indegrees[target_vertex_id]++;
        }

        std::queue<std::ptrdiff_t> vertex_ids_without_indegree;

        for (std::ptrdiff_t vertex_id : original_graph.vertex_ids())
        {
            if (indegrees[vertex_id] == 0)
            {
                vertex_ids_without_indegree.push(vertex_id);
            }
        }

        std::vector<std::ptrdiff_t> sorted_vertex_ids;

        while (!vertex_ids_without_indegree.empty())
        {
            std::ptrdiff_t vertex_id = vertex_ids_without_indegree.front();
            vertex_ids_without_indegree.pop();

            sorted_vertex_ids.push_back(vertex_id);

            for (const auto& edge_id : original_graph.edge_ids(vertex_id))
            {
                std::ptrdiff_t neighbor_vertex_id = original_graph.edge_target_vertex_id(edge_id);

                indegrees[neighbor_vertex_id]--;

                if (indegrees[neighbor_vertex_id] == 0)
                {
                    vertex_ids_without_indegree.push(neighbor_vertex_id);
                }
            }
        }

        return sorted_vertex_ids;
    }
};

#endif