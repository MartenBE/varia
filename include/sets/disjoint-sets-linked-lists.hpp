// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef DISJOINT_SETS_LINKED_LISTS_HPP
#define DISJOINT_SETS_LINKED_LISTS_HPP

#include <list>
#include <memory>
#include <unordered_map>
#include <unordered_set>

template <class T>
class disjoint_sets_linked_lists
{
private:
    using disjoint_set = std::list<T>;

public:
    void make_set(const T& member)
    {
        std::unique_ptr<disjoint_set> new_disjoint_set = std::make_unique<disjoint_set>();
        new_disjoint_set->push_back(member);
        m_member_to_disjoint_set[member] = new_disjoint_set.get();

        m_sets.insert(std::move(new_disjoint_set));

        m_amount_sets++;
    }

    void make_union(const T& member1, const T& member2)
    {
        disjoint_set* member1_disjoint_set = m_member_to_disjoint_set[member1];
        disjoint_set* member2_disjoint_set = m_member_to_disjoint_set[member2];

        assert(member1_disjoint_set);
        assert(member2_disjoint_set);

        if (member1_disjoint_set == member2_disjoint_set)
        {
            return;
        }

        if (member2_disjoint_set->size() > member1_disjoint_set->size()) // Weighted-union heuristic
        {
            swap(member1_disjoint_set, member2_disjoint_set);
        }

        for (const T& m : *member1_disjoint_set)
        {
            m_member_to_disjoint_set[m] = member2_disjoint_set;
        }

        member2_disjoint_set->splice(member2_disjoint_set->end(), *member1_disjoint_set);

        m_amount_sets--;
    }

    const T& find_set(const T& member) const
    {
        return m_member_to_disjoint_set.at(member)->front();
    }

    std::ptrdiff_t amount_sets() const
    {
        return m_amount_sets;
    }

private:
    std::unordered_set<std::unique_ptr<disjoint_set>> m_sets;
    std::unordered_map<T, disjoint_set*> m_member_to_disjoint_set;
    std::ptrdiff_t m_amount_sets{0};
};

#endif