// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef DISJOINT_SETS_FORESTS_HPP
#define DISJOINT_SETS_FORESTS_HPP

#include <unordered_map>

template <class T>
class disjoint_sets_forests
{
private:
    constexpr static std::ptrdiff_t no_parent = -1;

public:
    void make_set(const T& member)
    {
        m_parents[member] = no_parent;
        m_ranks[member] = 0;
        m_amount_sets++;
    }

    void make_union(const T& member1, const T& member2)
    {
        const T& root1 = find_set(member1);
        const T& root2 = find_set(member2);

        if (root1 == root2)
        {
            return;
        }

        // Union by rank. When unioning two trees of different height, we make
        // the root of the shorter tree a child of the root of the taller tree.
        // Importantly, this does not change the height of the taller tree. That
        // is, the rank of the taller tree does not change.
        //
        // However, when unioning two trees of the same height, we make one root
        // the child of the other, and this increases the height of that overall
        // tree by one, so we increase the rank of that root by one.
        //
        // ~ Chris Okasaki
        // https://stackoverflow.com/questions/18081539/disjoint-set-forests-why-should-the-rank-be-increased-by-one-when-the-find-of
        //
        if (m_ranks[root1] > m_ranks[root2])
        {
            m_parents[root2] = root1;
        }
        else
        {
            m_parents[root1] = root2;

            if (m_ranks[root1] == m_ranks[root2])
            {
                m_ranks[root1]++;
            }
        }

        m_amount_sets--;
    }

    const T& find_set(const T& member)
    {
        if (m_parents[member] == no_parent)
        {
            return member;
        }

        const T& root = find_set(m_parents[member]);

        // Path compression. This uses the unwinding stack from recusion. That's
        // why we have to use recursion above and cannot use a while loop
        // instead. It can be implemented using 2 while loops however, but we
        // stick with recursion here.
        //
        m_parents[member] = root;

        return root;
    }

    std::ptrdiff_t amount_sets() const
    {
        return m_amount_sets;
    }

private:
    std::unordered_map<T, T> m_parents;
    std::unordered_map<T, std::ptrdiff_t> m_ranks;
    std::ptrdiff_t m_amount_sets{0};
};

#endif