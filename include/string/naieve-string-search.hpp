// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef NAIEVE_STRING_SEARCH_HPP
#define NAIEVE_STRING_SEARCH_HPP

#include <string_view>
#include <vector>

class naieve_string_search
{
public:
    static std::vector<std::ptrdiff_t> search(const std::string_view& text, const std::string_view& pattern)
    {
        assert(!pattern.empty());

        std::vector<std::ptrdiff_t> found_indices;

        if (text.empty())
        {
            return found_indices;
        }

        for (std::ptrdiff_t i = 0; i <= (std::ssize(text) - std::ssize(pattern)); i++)
        {
            std::ptrdiff_t j = 0;
            while ((j < std::ssize(pattern)) && ((i + j) < std::ssize(text)) && (text[i + j] == pattern[j]))
            {
                j++;
            }

            if (j == std::ssize(pattern))
            {
                found_indices.push_back(i);
            }
        }

        return found_indices;
    }
};

#endif