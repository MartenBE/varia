// Copyright (C) 2022 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef LONGEST_COMMON_SUBSTRING_HPP
#define LONGEST_COMMON_SUBSTRING_HPP

#include "matrix.hpp"

#include <algorithm>
#include <string>
#include <string_view>

class longest_common_substring
{
private:
    enum class direction;

public:
    static std::string find(const std::string_view& lhs, const std::string_view& rhs)
    {
        matrix<int> lengths = calculate_lengths(lhs, rhs);

        return backtrack_lengths(lhs, rhs, lengths, std::ssize(lhs), std::ssize(rhs));
    }

private:
    static matrix<int> calculate_lengths(const std::string_view& lhs, const std::string_view& rhs)
    {
        matrix<int> lengths{(std::ssize(lhs) + 1), (std::ssize(rhs) + 1)};

        for (std::ptrdiff_t column = 0; column < lengths.amount_columns(); column++)
        {
            lengths(0, column) = 0;
        }

        for (std::ptrdiff_t row = 0; row < lengths.amount_rows(); row++)
        {
            lengths(row, 0) = 0;
        }

        for (std::ptrdiff_t row = 1; row < lengths.amount_rows(); row++)
        {
            for (std::ptrdiff_t column = 1; column < lengths.amount_columns(); column++)
            {
                std::ptrdiff_t new_length = -1;

                if (lhs[row - 1] == rhs[column - 1])
                {
                    new_length = (lengths((row - 1), (column - 1)) + 1);
                }
                else
                {
                    new_length = std::max(lengths(row - 1, column), lengths(row, column - 1));
                }

                lengths(row, column) = new_length;
            }
        }

        return lengths;
    }

    // This function is not polynomial, as it might branch in almost every step if the strings are similar.
    static std::string backtrack_lengths(const std::string_view& lhs,
                                         const std::string_view& rhs,
                                         const matrix<int>& lengths,
                                         std::ptrdiff_t row,
                                         std::ptrdiff_t column)
    {
        if (row == 0 || column == 0)
        {
            return "";
        }

        if (lhs[row - 1] == rhs[column - 1])
        {
            return (backtrack_lengths(lhs, rhs, lengths, (row - 1), (column - 1)) + lhs[row - 1]);
        }
        else
        {
            if (lengths(row - 1, column) > lengths(row, column - 1))
            {
                return backtrack_lengths(lhs, rhs, lengths, (row - 1), column);
            }
            else
            {
                return backtrack_lengths(lhs, rhs, lengths, row, (column - 1));
            }
        }
    }
};

#endif