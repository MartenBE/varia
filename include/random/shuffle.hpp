// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef SHUFFLE_HPP
#define SHUFFLE_HPP

#include <cassert>
#include <random>
#include <utility>

class shuffle
{
public:
    template <class random_access_iterator>
    static void naive(random_access_iterator begin, random_access_iterator end)
    {
        assert(begin <= end);

        const int amount_elements = std::distance(begin, end);

        if (amount_elements <= 1)
        {
            return;
        }

        std::random_device rd;
        std::mt19937 eng{rd()};
        std::uniform_int_distribution<> dist{0, (amount_elements - 1)};

        for (auto it = begin; it != end; it++)
        {
            std::iter_swap(it, (begin + dist(eng)));
        }
    }

    template <class random_access_iterator>
    static void durstenfeld(random_access_iterator begin, random_access_iterator end)
    {
        assert(begin <= end);

        if (std::distance(begin, end) <= 1)
        {
            return;
        }

        std::random_device rd;
        std::mt19937 eng{rd()};

        for (auto it = (end - 1); it != begin; it--)
        {
            const int distance = std::distance(begin, it);
            std::uniform_int_distribution<> dist{0, distance};

            std::iter_swap(it, (begin + dist(eng)));
        }
    }
};

#endif