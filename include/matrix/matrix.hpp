// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <algorithm>
#include <cassert>
#include <ostream>
#include <vector>

template <class T>
class matrix
{
    template <typename Y>
    friend class matrix;

public:
    matrix(std::ptrdiff_t amount_rows, std::ptrdiff_t amount_columns)
    : m_amount_rows{amount_rows},
      m_amount_columns{amount_columns},
      m_contents(amount_rows, std::vector<T>(amount_columns))
    {
    }

    matrix(std::ptrdiff_t amount_rows, std::ptrdiff_t amount_columns, const T& value)
    : m_amount_rows{amount_rows},
      m_amount_columns{amount_columns},
      m_contents(amount_rows, std::vector<T>(amount_columns, value))
    {
    }

    matrix(std::initializer_list<std::vector<T>> contents)
    {
        m_amount_rows = std::ssize(contents);

        if (m_amount_rows > 0)
        {
            int row_i = 0;
            for (const auto& row : contents)
            {
                if (row_i == 0)
                {
                    m_amount_columns = std::ssize(row);
                    m_contents = std::vector<std::vector<T>>(m_amount_rows, std::vector<T>(m_amount_columns));
                }

                assert(std::ssize(row) == m_amount_columns);
                m_contents[row_i] = std::vector<T>(row);
                row_i++;
            }
        }
    }

    matrix(const matrix& other) = default;
    matrix(matrix&& other) = default;
    matrix& operator=(const matrix& other) = default;
    matrix& operator=(matrix&& other) = default;
    virtual ~matrix() = default;

    // Calculations ////////////////////////////////////////////////////////////

    matrix transpose() const
    {
        matrix transposed_matrix{m_amount_columns, m_amount_rows};

        for (std::ptrdiff_t i = 0; i < m_amount_rows; i++)
        {
            for (std::ptrdiff_t j = 0; j < m_amount_columns; j++)
            {
                transposed_matrix(j, i) = this->operator()(i, j);
            }
        }

        return transposed_matrix;
    }

    static matrix<int> identity(std::ptrdiff_t size)
    {
        matrix identity_matrix{size, size};

        for (std::ptrdiff_t i = 0; i < size; i++)
        {
            for (std::ptrdiff_t j = 0; j < size; j++)
            {
                if (i == j)
                {
                    identity_matrix(i, j) = T{1};
                }
                else
                {
                    identity_matrix(i, j) = T{0};
                }
            }
        }

        return identity_matrix;
    }

    static matrix<int> ones(std::ptrdiff_t size)
    {
        matrix identity_matrix{size, size};

        return matrix<int>{size, size, T{1}};
    }

    static matrix<int> zeroes(std::ptrdiff_t size)
    {
        matrix identity_matrix{size, size};

        return matrix<int>{size, size, T{0}};
    }

    // Getters and setter //////////////////////////////////////////////////////

    int amount_rows() const
    {
        return m_amount_rows;
    }

    int amount_columns() const
    {
        return m_amount_columns;
    }

    // Operators ///////////////////////////////////////////////////////////////

    const T& operator()(std::ptrdiff_t row, std::ptrdiff_t column) const
    {
        assert(row < m_amount_rows);
        assert(column < m_amount_columns);

        return m_contents[row][column];
    }

    T& operator()(std::ptrdiff_t row, std::ptrdiff_t column)
    {
        assert(row < m_amount_rows);
        assert(column < m_amount_columns);

        return m_contents[row][column];
    }

    friend bool operator==(const matrix& lhs, const matrix& rhs)
    {
        if ((lhs.m_amount_rows != rhs.m_amount_rows) || (lhs.m_amount_columns != rhs.m_amount_columns))
        {
            return false;
        }

        for (std::ptrdiff_t i = 0; i < lhs.m_amount_rows; i++)
        {
            for (std::ptrdiff_t j = 0; j < lhs.m_amount_columns; j++)
            {
                if (lhs(i, j) != rhs(i, j))
                {
                    return false;
                }
            }
        }

        return true;
    }

    friend bool operator!=(const matrix& lhs, const matrix& rhs)
    {
        return !(lhs == rhs);
    }

    matrix& operator+=(const matrix& rhs)
    {
        assert(m_amount_rows == rhs.m_amount_rows);
        assert(m_amount_columns == rhs.m_amount_columns);

        for (std::ptrdiff_t i = 0; i < m_amount_rows; i++)
        {
            for (std::ptrdiff_t j = 0; j < m_amount_columns; j++)
            {
                this->operator()(i, j) += rhs(i, j);
            }
        }

        return (*this);
    }

    friend matrix operator+(matrix lhs, const matrix& rhs)
    {
        lhs += rhs;

        return lhs;
    }

    matrix& operator*=(double scalar)
    {
        for (std::ptrdiff_t i = 0; i < m_amount_rows; i++)
        {
            for (std::ptrdiff_t j = 0; j < m_amount_columns; j++)
            {
                this->operator()(i, j) *= scalar;
            }
        }

        return (*this);
    }

    friend matrix operator*(matrix lhs, double scalar)
    {
        lhs *= scalar;

        return lhs;
    }

    friend matrix operator*(double scalar, matrix rhs)
    {
        rhs *= scalar;

        return rhs;
    }

    friend matrix operator*(const matrix& lhs, const matrix& rhs)
    {
        assert(lhs.m_amount_columns == rhs.m_amount_rows);

        matrix result{lhs.m_amount_rows, rhs.m_amount_columns};

        for (std::ptrdiff_t result_row = 0; result_row < result.m_amount_rows; result_row++)
        {
            for (std::ptrdiff_t result_column = 0; result_column < result.m_amount_columns; result_column++)
            {
                T sum = T{0};

                for (std::ptrdiff_t i = 0; i < lhs.m_amount_columns; i++)
                {
                    sum += (lhs(result_row, i) * rhs(i, result_column));
                }

                result(result_row, result_column) = sum;
            }
        }

        return result;
    }

    // Streams /////////////////////////////////////////////////////////////////

    friend std::ostream& operator<<(std::ostream& os, const matrix& matrix)
    {
        os << "[";

        for (std::ptrdiff_t i = 0; i < matrix.m_amount_rows; i++)
        {
            os << "[";

            for (std::ptrdiff_t j = 0; j < matrix.m_amount_columns; j++)
            {
                os << matrix(i, j);

                if (j < (matrix.m_amount_columns - 1))
                {
                    os << ", ";
                }
            }

            os << "]";

            if (i < (matrix.m_amount_rows - 1))
            {
                os << ", ";
            }
        }

        os << "]";

        return os;
    }

private:
    std::ptrdiff_t m_amount_rows = 0;
    std::ptrdiff_t m_amount_columns = 0;

    std::vector<std::vector<T>> m_contents;
};

#endif