// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef SORTED_ARRAY_SEARCH_HPP
#define SORTED_ARRAY_SEARCH_HPP

#include <iterator>
#include <optional>

class sorted_array_search
{
public:
    template <class random_access_iterator, class T>
    static std::optional<random_access_iterator>
    linear_search(random_access_iterator begin, random_access_iterator end, const T& element_to_search)
    {
        if (std::distance(begin, end) == 0)
        {
            return std::nullopt;
        }

        auto it = begin;

        while (it != end && *it < element_to_search)
        {
            it++;
        }

        if (it != end && *it == element_to_search)
        {
            return std::optional<random_access_iterator>{it};
        }
        else
        {
            return std::nullopt;
        }
    }

    template <class random_access_iterator, class T>
    static std::optional<random_access_iterator>
    binary_search_iterative(random_access_iterator begin, random_access_iterator end, const T& element_to_search)
    {
        if (std::distance(begin, end) == 0)
        {
            return std::nullopt;
        }

        while (std::distance(begin, end) > 1)
        {
            auto mid = (begin + (std::distance(begin, end) / 2));

            if (element_to_search < *mid)
            {
                end = mid;
            }
            else
            {
                begin = mid;
            }
        }

        if (*begin == element_to_search)
        {
            return std::optional<random_access_iterator>{begin};
        }
        else
        {
            return std::nullopt;
        }
    }

    template <class random_access_iterator, class T>
    static std::optional<random_access_iterator>
    binary_search_recursive(random_access_iterator begin, random_access_iterator end, const T& element_to_search)
    {
        if (std::distance(begin, end) == 0)
        {
            return std::nullopt;
        }
        else if (std::distance(begin, end) == 1)
        {
            if (*begin == element_to_search)
            {
                return std::optional<random_access_iterator>{begin};
            }
            else
            {
                return std::nullopt;
            }
        }

        auto mid = (begin + (std::distance(begin, end) / 2));

        if (element_to_search < *mid)
        {
            return binary_search_recursive(begin, mid, element_to_search);
        }
        else
        {
            return binary_search_recursive(mid, end, element_to_search);
        }
    }
};

#endif