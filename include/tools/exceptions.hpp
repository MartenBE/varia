// Copyright (C) 2021 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef EXCEPTIONS_HPP
#define EXCEPTIONS_HPP

#include <stdexcept>

class not_implemented_exception : public std::logic_error
{
public:
    not_implemented_exception() : std::logic_error{"Function not yet implemented."}
    {
    }
};

#endif