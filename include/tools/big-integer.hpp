// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef BIG_INTEGER_HPP
#define BIG_INTEGER_HPP

#include <cassert>
#include <cmath>
#include <cstdint>
#include <string>
#include <vector>

// This implementation is based on the OpenJdk BigInteger class.
// An explanation of the OpenJdk BigInteger class can be found on
// http://www.programmersought.com/article/6631135031/ and
// https://geekofficedog.blogspot.com/2015/07/lets-code-big-integer-part-i.html .
// The source of the OpenJdk BigInteger class can be found on
// https://hg.openjdk.java.net/jdk/jdk11/file/tip/src/java.base/share/classes/java/math/BigInteger.java .

class big_integer
{
public:
    big_integer(const std::string& number)
    {
        assert(!number.empty());

        int cursor = 0;

        if (number[cursor] == '-')
        {
            is_positive = false;
            cursor++;
        }

        while (number[cursor] == '0')
        {
            cursor++;
        }

        assert(cursor < number.length());

        const int amount_digits_per_int =
                static_cast<int>(std::floor(std::log10(std::numeric_limits<uint32_t>::max())));

        bit_words.resize(static_cast<int>(
                std::ceil(static_cast<double>(number.length() - cursor) / static_cast<double>(amount_digits_per_int))));

        const int amount_digits_first_word = ((number.length() - cursor) % amount_digits_per_int);
        bit_words[0] = std::stol(number.substr(cursor, amount_digits_first_word));
        cursor += amount_digits_first_word;

        const uint64_t factor = static_cast<uint64_t>(std::pow(10, amount_digits_per_int));

        int bit_word_index = 1;
        while (cursor < number.length())
        {
            uint64_t carry = 0;

            for (int i = 0; i < bit_word_index; i++)
            {
                uint64_t current_bit_word = static_cast<uint64_t>(bit_words[i]);
                uint64_t current_value = std::stol(number.substr(cursor, amount_digits_per_int));
                current_bit_word = ((current_bit_word * factor) + current_value + carry);

                bit_words[i] = (current_bit_word % std::numeric_limits<uint32_t>::max());
                carry = (current_bit_word / std::numeric_limits<uint32_t>::max());
            }

            if (carry != 0)
            {
                bit_words[bit_word_index] = static_cast<uint32_t>(carry);
            }

            cursor += amount_digits_per_int;
            bit_word_index++;
        }

        // TODO remove leading zeroes

        assert(cursor == number.length());
    }

private:
    bool is_positive = true;
    std::vector<uint32_t> bit_words;
};

#endif