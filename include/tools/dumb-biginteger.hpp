// Copyright (C) 2021 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef DUMB_BIGINTEGER_HPP
#define DUMB_BIGINTEGER_HPP

#include "exceptions.hpp"
#include "simple-functions.hpp"

#include <string>
#include <vector>

// This is a very very basic, dumb and slow way to represent a big integer.
//
class dumb_biginteger
{
private:
    using digit = short int;

public:
    dumb_biginteger() : dumb_biginteger{"0"}
    {
    }

    dumb_biginteger(long long int number)
    {
        int amounts = simple_functions::amount_digits(number);
        for (int i = 0; i < amounts; i++)
        {
            digit new_digit = number % 10;
            m_digits.push_back(new_digit);
            number /= 10;
        }
    }

    dumb_biginteger(const std::string& number)
    {
        std::ptrdiff_t position = 0;

        if (number[position] == '-')
        {
            m_is_negative = true;
            position++;
        }
        else if (number[position] == '+')
        {
            position++;
        }

        while (number[position] == '0')
        {
            position++;
        }

        if (position == std::ssize(number))
        {
            // The number exists only of zeroes
            m_digits.push_back(0);
            m_is_negative = false;
        }
        else
        {
            for (std::ptrdiff_t i = (std::ssize(number) - 1); i >= position; i--)
            {
                digit new_digit = number[i] - '0';
                m_digits.push_back(new_digit); // TODO reserve
            }
        }
    }

    dumb_biginteger(const dumb_biginteger&) = default;
    dumb_biginteger& operator=(const dumb_biginteger&) = default;
    dumb_biginteger(dumb_biginteger&&) = default;
    dumb_biginteger& operator=(dumb_biginteger&&) = default;
    virtual ~dumb_biginteger() = default;

    // All math operators

    friend bool operator==(const dumb_biginteger& lhs, const dumb_biginteger& rhs)
    {
        return ((lhs.m_digits == rhs.m_digits) && (lhs.m_is_negative == rhs.m_is_negative));
    }

    friend bool operator!=(const dumb_biginteger& lhs, const dumb_biginteger& rhs)
    {
        return !(lhs == rhs);
    }

    friend dumb_biginteger operator+(const dumb_biginteger& lhs, const dumb_biginteger& rhs)
    {
        dumb_biginteger result;

        if (lhs.m_is_negative == rhs.m_is_negative) // pos + pos or neg + neg
        {
            result.m_digits = add_absolute_values(lhs.m_digits, rhs.m_digits);
            result.m_is_negative = lhs.m_is_negative;
        }
        else
        {
            result.m_digits = subtract_absolute_values(lhs.m_digits, rhs.m_digits);
            if (!lhs.m_is_negative) // neg + pos
            {
                result.m_is_negative =
                        (compare_absolute_values(lhs.m_digits, rhs.m_digits) == std::strong_ordering::less);
            }
            else // pos + neg
            {
                result.m_is_negative =
                        (compare_absolute_values(lhs.m_digits, rhs.m_digits) == std::strong_ordering::greater);
            }
        }

        return result;
    }

    friend dumb_biginteger operator-(const dumb_biginteger& lhs, const dumb_biginteger& rhs)
    {
        dumb_biginteger result;

        if (lhs.m_is_negative != rhs.m_is_negative) // pos - neg or neg - pos
        {
            result.m_digits = add_absolute_values(lhs.m_digits, rhs.m_digits);
            result.m_is_negative = lhs.m_is_negative;
        }
        else
        {
            result.m_digits = subtract_absolute_values(lhs.m_digits, rhs.m_digits);
            if (!lhs.m_is_negative) // pos - pos
            {
                result.m_is_negative =
                        (compare_absolute_values(lhs.m_digits, rhs.m_digits) == std::strong_ordering::less);
            }
            else // neg - neg
            {
                result.m_is_negative =
                        (compare_absolute_values(lhs.m_digits, rhs.m_digits) == std::strong_ordering::greater);
            }
        }

        return result;
    }

    friend std::ostream& operator<<(std::ostream& os, const dumb_biginteger& number)
    {
        if (number.m_is_negative)
        {
            os << '-';
        }

        for (std::ptrdiff_t i = (std::ssize(number.m_digits) - 1); i >= 0; i--)
        {
            os << number.m_digits[i];
        }

        return os;
    }

    std::ptrdiff_t amounts() const
    {
        return std::ssize(m_digits);
    }

private:
    static std::strong_ordering compare_absolute_values(const std::vector<digit>& lhs, const std::vector<digit>& rhs)
    {
        if (std::ssize(lhs) == std::ssize(rhs))
        {
            std::ptrdiff_t i = (std::ssize(lhs) - 1);
            while (i >= 0 && lhs[i] == rhs[i])
            {
                i--;
            }

            if (i < 0)
            {
                return std::strong_ordering::equal;
            }
            else if (lhs[i] < rhs[i])
            {
                return std::strong_ordering::less;
            }
            else
            {
                return std::strong_ordering::greater;
            }
        }
        else if (std::ssize(lhs) < std::ssize(rhs))
        {
            return std::strong_ordering::less;
        }
        else
        {
            return std::strong_ordering::greater;
        }
    }

    static std::vector<digit> add_absolute_values(const std::vector<digit>& lhs, const std::vector<digit>& rhs)
    {
        // We have to work with pointers, otherwise we can't swap
        const auto* largest_abs_value = &lhs;
        const auto* lowest_abs_value = &rhs;

        if (compare_absolute_values(*lowest_abs_value, *largest_abs_value) == std::strong_ordering::greater)
        {
            std::swap(largest_abs_value, lowest_abs_value);
        }

        std::vector<digit> result;

        digit carry = 0;
        for (std::ptrdiff_t i = 0; i < std::ssize(*largest_abs_value); i++)
        {
            digit sum = (*largest_abs_value)[i];
            if (i < std::ssize(*lowest_abs_value))
            {
                sum += (*lowest_abs_value)[i];
            }
            sum += carry;

            carry = (sum / 10);
            result.push_back(sum % 10);
        }

        if (carry != 0)
        {
            result.push_back(carry);
        }

        remove_leading_zeros(result);

        return result;
    }

    // Minuend - substrahend == difference
    static std::vector<digit> subtract_absolute_values(const std::vector<digit>& lhs, const std::vector<digit>& rhs)
    {
        // We have to work with pointers, otherwise we can't swap
        const auto* largest_abs_value = &lhs;
        const auto* lowest_abs_value = &rhs;

        if (compare_absolute_values(*lowest_abs_value, *largest_abs_value) == std::strong_ordering::greater)
        {
            std::swap(largest_abs_value, lowest_abs_value);
        }

        std::vector<digit> result;

        digit carry = 0;
        for (std::ptrdiff_t i = 0; i < std::ssize(*largest_abs_value); i++)
        {
            digit minuend = (*largest_abs_value)[i];
            digit substrahend;
            if (i < std::ssize(*lowest_abs_value))
            {
                substrahend = (*lowest_abs_value)[i];
            }
            else
            {
                substrahend = 0;
            }

            minuend -= carry;

            if (minuend >= substrahend)
            {
                carry = 0;
            }
            else
            {
                minuend += 10;
                carry = 1;
            }

            result.push_back(minuend - substrahend);
        }

        remove_leading_zeros(result);

        return result;
    }

    static void remove_leading_zeros(std::vector<digit>& digits)
    {
        assert(!digits.empty());

        std::ptrdiff_t i = (digits.size() - 1);
        if (digits[i] != 0)
        {
            return;
        }

        while (i >= 0 && digits[i] == 0)
        {
            i--;
        }

        if (i != -1)
        {
            digits.resize(i + 1);
        }
        else
        {
            digits = {0};
        }
    }

    std::vector<digit> m_digits;
    bool m_is_negative = false;
};

#endif
