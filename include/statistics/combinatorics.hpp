// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef COMBINATORICS_HPP
#define COMBINATORICS_HPP

int permutation(int n);
int variation(int n, int p);
int variation_repetition(int n, int p);
int combination(int n, int p);
int combination_repetition(int n, int p);

#endif