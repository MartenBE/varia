// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef HEAPSORT_HPP
#define HEAPSORT_HPP

#include "binary-heap.hpp"

#include <iterator>

template <class T>
class heapsort
{
public:
    template <class random_access_iterator>
    void operator()(random_access_iterator begin, random_access_iterator end) const
    {
        assert(begin <= end);

        if (std::distance(begin, end) <= 1)
        {
            return;
        }

        binary_heap<T>::heapify(begin, end);

        auto heap_end = end;
        while (heap_end > (begin + 1))
        {
            heap_end--;
            std::iter_swap(begin, heap_end);
            binary_heap<T>::top_down_reheapify(begin, heap_end, begin);
        }
    }
};

#endif