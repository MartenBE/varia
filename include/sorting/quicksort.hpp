// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef QUICKSORT_HPP
#define QUICKSORT_HPP

#include <algorithm>

template <class T>
class quicksort
{
public:
    template <class random_access_iterator>
    void operator()(random_access_iterator begin, random_access_iterator end) const
    {
        quicksort_recursive_step(begin, end);
    }

private:
    template <class random_access_iterator>
    void quicksort_recursive_step(random_access_iterator begin, random_access_iterator end) const
    {
        assert(begin <= end);

        if ((end - begin) <= 1)
        {
            return;
        }

        auto left = begin;
        auto right = (end - 1);
        auto middle = (left + ((right - left) / 2));

        if (*left > *middle)
        {
            std::iter_swap(left, middle);
        }

        if (*middle > *right)
        {
            std::iter_swap(middle, right);
        }

        if (*left > *middle)
        {
            std::iter_swap(left, middle);
        }

        const auto pivot = *middle;

        while (*left < pivot)
        {
            left++;
        }

        while (*right > pivot)
        {
            right--;
        }

        while (left < right)
        {
            std::iter_swap(left, right);

            left++;
            while (*left < pivot)
            {
                left++;
            }

            right--;
            while (*right > pivot)
            {
                right--;
            }
        }

        auto bound = (right + 1);
        quicksort_recursive_step(begin, bound);
        quicksort_recursive_step(bound, end);
    }
};

#endif