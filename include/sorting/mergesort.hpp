// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef MERGESORT_HPP
#define MERGESORT_HPP

#include <algorithm>
#include <iterator>

template <class T>
class mergesort
{
public:
    template <class random_access_iterator>
    void operator()(random_access_iterator begin, random_access_iterator end)
    {
        assert(begin <= end);

        const auto amount_elements = std::distance(begin, end);

        if (amount_elements <= 1)
        {
            return;
        }

        auxiliary_array.resize(amount_elements);
        sort(begin, end, auxiliary_array.begin(), auxiliary_array.end());
    }

private:
    template <class random_access_iterator>
    void sort(random_access_iterator begin,
              random_access_iterator end,
              random_access_iterator begin_aux,
              random_access_iterator end_aux)
    {
        if (std::distance(begin, end) <= 1)
        {
            return;
        }

        auto middle = get_middle_iterator(begin, end);
        auto middle_aux = get_middle_iterator(begin_aux, end_aux);

        sort(begin, middle, begin_aux, middle_aux);
        sort(middle, end, middle_aux, end_aux);

        merge(begin, middle, end, begin_aux, end_aux);
    }

    template <class random_access_iterator>
    void merge(random_access_iterator begin,
               random_access_iterator middle,
               random_access_iterator end,
               random_access_iterator begin_aux,
               random_access_iterator end_aux)
    {
        auto left = begin;
        auto right = middle;
        auto temp = begin_aux;

        while ((left < middle) && (right < end))
        {
            if (*left <= *right)
            {
                *temp = *left;
                left++;
            }
            else
            {
                *temp = *right;
                right++;
            }
            temp++;
        }

        while (left < middle)
        {
            *temp = *left;
            left++;
            temp++;
        }

        while (right < end)
        {
            *temp = *right;
            right++;
            temp++;
        }

        std::copy(begin_aux, end_aux, begin);
    }

    template <class random_access_iterator>
    random_access_iterator get_middle_iterator(random_access_iterator begin, random_access_iterator end)
    {
        return (begin + ((end - begin) / 2));
    }

    std::vector<T> auxiliary_array;
};

#endif