// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef BUBBLESORT_HPP
#define BUBBLESORT_HPP

#include <cassert>
#include <iterator>

template <class T>
class bubblesort
{
public:
    template <class random_access_iterator>
    void operator()(random_access_iterator begin, random_access_iterator end) const
    {
        assert(begin <= end);

        if (std::distance(begin, end) <= 1)
        {
            return;
        }

        for (auto it = begin; it != (end - 1); it++)
        {
            for (auto reverse_it = (end - 1); reverse_it > it; reverse_it--)
            {
                if (*(reverse_it - 1) > *(reverse_it))
                {
                    std::swap(*(reverse_it - 1), *(reverse_it));
                }
            }
        }
    }
};

#endif