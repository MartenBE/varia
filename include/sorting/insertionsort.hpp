// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef INSERTIONSORT_HPP
#define INSERTIONSORT_HPP

#include <cassert>
#include <iterator>

template <class T>
class insertionsort
{
public:
    template <class random_access_iterator>
    void operator()(random_access_iterator begin, random_access_iterator end) const
    {
        assert(begin <= end);

        if (std::distance(begin, end) <= 1)
        {
            return;
        }

        random_access_iterator to_insert = (begin + 1);

        while (to_insert < end)
        {
            T to_insert_value = std::move(*to_insert);
            random_access_iterator it = to_insert;

            while ((it > begin) && (to_insert_value < *(it - 1)))
            {
                *it = std::move(*(it - 1));
                it--;
            }

            *it = std::move(to_insert_value);
            to_insert++;
        }
    }
};

#endif