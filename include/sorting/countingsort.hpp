// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef COUNTINGSORT_HPP
#define COUNTINGSORT_HPP

#include <algorithm>
#include <iterator>
#include <vector>

template <class T>
class countingsort
{
public:
    template <class random_access_iterator>
    void operator()(random_access_iterator begin, random_access_iterator end) const
    {
        assert(begin <= end);

        const auto amount_elements = std::distance(begin, end);

        if (amount_elements <= 1)
        {
            return;
        }

        const T greatest_element = find_greatest_element(begin, end);
        std::vector<int> frequencies((greatest_element + 1), 0);

        for (auto it = begin; it != end; it++)
        {
            frequencies[*it]++;
        }

        for (auto it = (frequencies.begin() + 1); it != frequencies.end(); it++)
        {
            *it += *(it - 1);
        }

        std::vector<T> output(amount_elements);

        for (auto it = begin; it != end; it++)
        {
            output[frequencies[*it] - 1] = *it;
            frequencies[*it]--;
        }

        std::copy(output.begin(), output.end(), begin);
    }

private:
    template <class random_access_iterator>
    T find_greatest_element(random_access_iterator begin, random_access_iterator end) const
    {
        T greatest = T{0};

        for (auto it = begin; it != end; it++)
        {
            if (*it > greatest)
            {
                greatest = *it;
            }
        }

        return greatest;
    }
};

#endif