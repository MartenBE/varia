// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#ifndef SELECTIONSORT_HPP
#define SELECTIONSORT_HPP

#include <cassert>
#include <iterator>

template <class T>
class selectionsort
{
public:
    template <class random_access_iterator>
    void operator()(random_access_iterator begin, random_access_iterator end) const
    {
        assert(begin <= end);

        if (std::distance(begin, end) <= 1)
        {
            return;
        }

        random_access_iterator to_replace = begin;

        while (to_replace < (end - 1))
        {
            random_access_iterator it = to_replace;
            random_access_iterator min = it;

            it++;
            while (it < end)
            {
                if (*it < *min)
                {
                    min = it;
                }

                it++;
            }

            if (to_replace != min)
            {
                std::swap(*to_replace, *min);
            }

            to_replace++;
        }
    }
};

#endif