// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "binary-heap.hpp"
#include "binomial-heap.hpp"
#include "catch2/catch_template_test_macros.hpp"

#include <functional>
#include <queue>
#include <vector>

TEMPLATE_TEST_CASE(
        "Test priority queue structures", "", (std::priority_queue<int>), (binary_heap<int>), (binomial_heap<int>) )
{
    std::vector<int> data{10, 5, 13, 17, 9, 7, 20, 12, 14, 18};
    std::vector<int> expected_top_values{10, 10, 13, 17, 17, 17, 20, 20, 20, 20};

    TestType pq;

    REQUIRE(pq.empty());

    for (std::ptrdiff_t i = 0; i < std::ssize(data); i++)
    {
        pq.push(data[i]);
        REQUIRE(std::ssize(pq) == (i + 1));
        REQUIRE(pq.top() == expected_top_values[i]);
    }

    REQUIRE(!pq.empty());
    REQUIRE(std::ssize(pq) == std::ssize(data));

    SECTION("Top and pop")
    {
        std::vector<int> expected_top_values{20, 18, 17, 14, 13, 12, 10, 9, 7, 5};

        for (std::ptrdiff_t i = 0; i < std::ssize(data); i++)
        {
            REQUIRE(pq.top() == expected_top_values[i]);
            REQUIRE(std::ssize(pq) == (std::ssize(data) - i));
            pq.pop();
        }

        REQUIRE(pq.empty());
        REQUIRE(std::ssize(pq) == 0);
    }
}