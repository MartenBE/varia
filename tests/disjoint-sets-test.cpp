// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "catch2/catch_template_test_macros.hpp"
#include "disjoint-sets-forests.hpp"
#include "disjoint-sets-linked-lists.hpp"

TEMPLATE_TEST_CASE("Find connected components using disjoint sets (example CLRS)",
                   "",
                   (disjoint_sets_linked_lists<char>),
                   (disjoint_sets_forests<char>) )
{
    TestType ds;

    ds.make_set('a');
    ds.make_set('b');
    ds.make_set('c');
    ds.make_set('d');
    ds.make_set('e');
    ds.make_set('f');
    ds.make_set('g');
    ds.make_set('h');
    ds.make_set('i');
    ds.make_set('j');

    ds.make_union('b', 'd');
    ds.make_union('e', 'g');
    ds.make_union('a', 'c');
    ds.make_union('h', 'i');
    ds.make_union('a', 'b');
    ds.make_union('e', 'f');
    ds.make_union('b', 'c');

    SECTION("Union")
    {
        // {{'a', 'b', 'c', 'd'}, {'e', 'f', 'g'}, {'h', 'i'}, {'j'}}

        REQUIRE(ds.find_set('a') == ds.find_set('a'));
        REQUIRE(ds.find_set('b') == ds.find_set('a'));
        REQUIRE(ds.find_set('c') == ds.find_set('a'));
        REQUIRE(ds.find_set('d') == ds.find_set('a'));
        REQUIRE(ds.find_set('e') == ds.find_set('e'));
        REQUIRE(ds.find_set('f') == ds.find_set('e'));
        REQUIRE(ds.find_set('g') == ds.find_set('e'));
        REQUIRE(ds.find_set('h') == ds.find_set('h'));
        REQUIRE(ds.find_set('i') == ds.find_set('h'));
        REQUIRE(ds.find_set('j') == ds.find_set('j'));
    }

    SECTION("Amount sets")
    {
        REQUIRE(ds.amount_sets() == 4);
    }

    SECTION("Weighted-union heuristic")
    {
        ds.make_union('e', 'd');

        // {{'a', 'b', 'c', 'd', 'e', 'f', 'g'}, {'h', 'i'}, {'j'}}

        REQUIRE(ds.find_set('a') == ds.find_set('a'));
        REQUIRE(ds.find_set('b') == ds.find_set('a'));
        REQUIRE(ds.find_set('c') == ds.find_set('a'));
        REQUIRE(ds.find_set('d') == ds.find_set('a'));
        REQUIRE(ds.find_set('e') == ds.find_set('a'));
        REQUIRE(ds.find_set('f') == ds.find_set('a'));
        REQUIRE(ds.find_set('g') == ds.find_set('a'));
        REQUIRE(ds.find_set('h') == ds.find_set('h'));
        REQUIRE(ds.find_set('i') == ds.find_set('h'));
        REQUIRE(ds.find_set('j') == ds.find_set('j'));
    }
}