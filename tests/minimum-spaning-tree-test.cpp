// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "catch2/catch_template_test_macros.hpp"
#include "kruskal.hpp"
#include "prim.hpp"

TEMPLATE_TEST_CASE("Create a minimum cost spanning tree", "", (prim<std::string, int>), (kruskal<std::string, int>) )
{
    SECTION("Example Skiena")
    {
        graph<std::string, int> original_graph;

        int a = original_graph.add_vertex("a");
        int b = original_graph.add_vertex("b");
        int c = original_graph.add_vertex("c");
        int d = original_graph.add_vertex("d");
        int e = original_graph.add_vertex("e");
        int f = original_graph.add_vertex("f");
        int g = original_graph.add_vertex("g");

        original_graph.add_undirected_edge(a, b, 5);
        original_graph.add_undirected_edge(a, e, 7);
        original_graph.add_undirected_edge(a, g, 12);
        original_graph.add_undirected_edge(b, c, 7);
        original_graph.add_undirected_edge(b, e, 9);
        original_graph.add_undirected_edge(c, d, 5);
        original_graph.add_undirected_edge(c, e, 4);
        original_graph.add_undirected_edge(c, f, 2);
        original_graph.add_undirected_edge(d, f, 2);
        original_graph.add_undirected_edge(e, f, 3);
        original_graph.add_undirected_edge(e, g, 4);
        original_graph.add_undirected_edge(f, g, 7);

        TestType mst_generator{original_graph};

        REQUIRE(mst_generator.sum_edge_weights() == 23);
    }

    SECTION("Example CLRS")
    {
        graph<std::string, int> original_graph;

        int a = original_graph.add_vertex("a");
        int b = original_graph.add_vertex("b");
        int c = original_graph.add_vertex("c");
        int d = original_graph.add_vertex("d");
        int e = original_graph.add_vertex("e");
        int f = original_graph.add_vertex("f");
        int g = original_graph.add_vertex("g");
        int h = original_graph.add_vertex("h");
        int i = original_graph.add_vertex("i");

        original_graph.add_undirected_edge(a, b, 4);
        original_graph.add_undirected_edge(a, h, 8);
        original_graph.add_undirected_edge(b, c, 8);
        original_graph.add_undirected_edge(b, h, 11);
        original_graph.add_undirected_edge(c, d, 7);
        original_graph.add_undirected_edge(c, f, 4);
        original_graph.add_undirected_edge(c, i, 2);
        original_graph.add_undirected_edge(d, e, 9);
        original_graph.add_undirected_edge(d, f, 14);
        original_graph.add_undirected_edge(e, f, 10);
        original_graph.add_undirected_edge(f, g, 2);
        original_graph.add_undirected_edge(g, h, 1);
        original_graph.add_undirected_edge(g, i, 6);
        original_graph.add_undirected_edge(h, i, 7);

        TestType mst_generator{original_graph};

        REQUIRE(mst_generator.sum_edge_weights() == 37);
    }
}