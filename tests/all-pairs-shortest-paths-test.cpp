// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "catch2/catch_test_macros.hpp"
#include "floyd-warshall.hpp"

TEST_CASE("Find shortest paths between pair of vertices using Floyd-Warshall's algorithm")
{
    graph<std::string, int> g;

    int a = g.add_vertex("a");
    int b = g.add_vertex("b");
    int c = g.add_vertex("c");
    int d = g.add_vertex("d");
    int e = g.add_vertex("e");

    g.add_edge(a, b, 3);
    g.add_edge(a, c, 8);
    g.add_edge(a, e, -4);
    g.add_edge(b, d, 1);
    g.add_edge(b, e, 7);
    g.add_edge(c, b, 4);
    g.add_edge(d, a, 2);
    g.add_edge(d, c, -5);
    g.add_edge(e, d, 6);

    floyd_warshall<std::string, int> fw{g};

    SECTION("Calculate distances")
    {
        REQUIRE(fw.distances()
                == matrix<int>{
                        {0, 1, -3, 2, -4}, {3, 0, -4, 1, -1}, {7, 4, 0, 5, 3}, {2, -1, -5, 0, -2}, {8, 5, 1, 6, 0}});
    }
}