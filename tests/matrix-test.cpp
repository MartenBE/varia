// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "catch2/catch_test_macros.hpp"
#include "matrix.hpp"

#include <sstream>

TEST_CASE("Matrix can be initialized with nested initializer lists")
{
    matrix<int> test = {{1, 2, 3}, {11, 22, 33}, {111, 222, 333}};

    REQUIRE(test(0, 0) == 1);
    REQUIRE(test(0, 1) == 2);
    REQUIRE(test(0, 2) == 3);
    REQUIRE(test(1, 0) == 11);
    REQUIRE(test(1, 1) == 22);
    REQUIRE(test(1, 2) == 33);
    REQUIRE(test(2, 0) == 111);
    REQUIRE(test(2, 1) == 222);
    REQUIRE(test(2, 2) == 333);
}

TEST_CASE("Generate identity matrices")
{
    REQUIRE(matrix<int>::identity(0) == matrix<int>{});
    REQUIRE(matrix<int>::identity(1) == matrix<int>{{1}});
    REQUIRE(matrix<int>::identity(3) == matrix<int>{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}});
    REQUIRE(matrix<int>::identity(8)
            == matrix<int>{{1, 0, 0, 0, 0, 0, 0, 0},
                           {0, 1, 0, 0, 0, 0, 0, 0},
                           {0, 0, 1, 0, 0, 0, 0, 0},
                           {0, 0, 0, 1, 0, 0, 0, 0},
                           {0, 0, 0, 0, 1, 0, 0, 0},
                           {0, 0, 0, 0, 0, 1, 0, 0},
                           {0, 0, 0, 0, 0, 0, 1, 0},
                           {0, 0, 0, 0, 0, 0, 0, 1}});
}

TEST_CASE("Generate zero matrices")
{
    REQUIRE(matrix<int>::zeroes(0) == matrix<int>{});
    REQUIRE(matrix<int>::zeroes(1) == matrix<int>{{0}});
    REQUIRE(matrix<int>::zeroes(3) == matrix<int>{{0, 0, 0}, {0, 0, 0}, {0, 0, 0}});
    REQUIRE(matrix<int>::zeroes(8)
            == matrix<int>{{0, 0, 0, 0, 0, 0, 0, 0},
                           {0, 0, 0, 0, 0, 0, 0, 0},
                           {0, 0, 0, 0, 0, 0, 0, 0},
                           {0, 0, 0, 0, 0, 0, 0, 0},
                           {0, 0, 0, 0, 0, 0, 0, 0},
                           {0, 0, 0, 0, 0, 0, 0, 0},
                           {0, 0, 0, 0, 0, 0, 0, 0},
                           {0, 0, 0, 0, 0, 0, 0, 0}});
}

TEST_CASE("Generate ones matrices")
{
    REQUIRE(matrix<int>::ones(0) == matrix<int>{});
    REQUIRE(matrix<int>::ones(1) == matrix<int>{{1}});
    REQUIRE(matrix<int>::ones(3) == matrix<int>{{1, 1, 1}, {1, 1, 1}, {1, 1, 1}});
    REQUIRE(matrix<int>::ones(8)
            == matrix<int>{{1, 1, 1, 1, 1, 1, 1, 1},
                           {1, 1, 1, 1, 1, 1, 1, 1},
                           {1, 1, 1, 1, 1, 1, 1, 1},
                           {1, 1, 1, 1, 1, 1, 1, 1},
                           {1, 1, 1, 1, 1, 1, 1, 1},
                           {1, 1, 1, 1, 1, 1, 1, 1},
                           {1, 1, 1, 1, 1, 1, 1, 1},
                           {1, 1, 1, 1, 1, 1, 1, 1}});
}

TEST_CASE("Transpose matrix")
{
    matrix<int> test{{1, 2, 3, 4}, {5, 6, 7, 8}};

    REQUIRE(test.transpose() == matrix<int>{{1, 5}, {2, 6}, {3, 7}, {4, 8}});
}

TEST_CASE("Print matrix")
{
    matrix<int> test{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}};
    std::string expected = "[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]";

    std::stringstream out;
    out << test;

    REQUIRE(out.str() == expected);
}

TEST_CASE("Multiplicate with scalar")
{
    matrix<int> test{{5, 0, 10, 0, 15}, {0, -3, -15, 2, 18}, {10, 0, 20, 0, 30}, {-5, -3, -25, 0, -9}};
    int scalar = -1;
    matrix<int> expected{{-5, 0, -10, 0, -15}, {0, 3, 15, -2, -18}, {-10, 0, -20, 0, -30}, {5, 3, 25, 0, 9}};

    REQUIRE((test * scalar) == expected);
    REQUIRE((scalar * test) == expected);
}

TEST_CASE("Multiplicate with another matrix")
{
    matrix<int> lhs{{2, 1, 4}, {0, 1, 1}};
    matrix<int> rhs{{6, 3, -1, 0}, {1, 1, 0, 4}, {-2, 5, 0, 2}};
    matrix<int> expected{{5, 27, -2, 12}, {-1, 6, 0, 6}};

    REQUIRE((lhs * rhs) == expected);
}

TEST_CASE("Test equality with another matrix")
{
    REQUIRE(matrix<int>{} == matrix<int>{});
    REQUIRE(matrix<int>{{1, 1}, {1, 1}} == matrix<int>{{1, 1}, {1, 1}});
    REQUIRE(matrix<int>{{1, 1}, {1, 1}} != matrix<int>{{1, 1}, {1, 1}, {1, 1}});
    REQUIRE(matrix<int>{{1, 1}, {1, 1}} != matrix<int>{{1, 1, 1}, {1, 1, 1}});
    REQUIRE(matrix<int>{{1, 1}, {1, 1}} != matrix<int>{});
    REQUIRE(matrix<int>{{1, 1}, {1, 1}} != matrix<int>{{2, 1}, {1, 1}});
    REQUIRE(matrix<int>{{1, 1}, {1, 1}} != matrix<int>{{1, 2}, {1, 1}});
    REQUIRE(matrix<int>{{1, 1}, {1, 1}} != matrix<int>{{1, 1}, {2, 1}});
    REQUIRE(matrix<int>{{1, 1}, {1, 1}} != matrix<int>{{1, 1}, {1, 2}});
}