// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "catch2/catch_test_macros.hpp"
#include "dijkstra.hpp"

TEST_CASE("Find shortest path between 2 vertices using Dijkstra's algorithm")
{
    SECTION("Path construction")
    {
        graph<std::string, int> g;

        int a = g.add_vertex("a");
        int b = g.add_vertex("b");
        int c = g.add_vertex("c");
        int d = g.add_vertex("d");
        int e = g.add_vertex("e");

        g.add_undirected_edge(a, b, 1);
        g.add_undirected_edge(b, c, 1);
        g.add_undirected_edge(c, d, 1);
        g.add_undirected_edge(d, e, 1);

        dijkstra<std::string, int> searcher{g, a};
        auto result = searcher.search_path(e);

        REQUIRE(result.has_value());
        REQUIRE(result.value().first == std::vector<std::ptrdiff_t>{a, b, c, d, e});
        REQUIRE(result.value().second == 4);
    }

    SECTION("Path to itself")
    {
        graph<std::string, int> g;

        int a = g.add_vertex("a");

        dijkstra<std::string, int> searcher{g, a};
        auto result = searcher.search_path(a);

        REQUIRE(result.has_value());
        REQUIRE(result.value().first == std::vector<std::ptrdiff_t>{a});
        REQUIRE(result.value().second == 0);
    }

    SECTION("No direct path")
    {
        graph<std::string, int> g;

        int a = g.add_vertex("a");
        int b = g.add_vertex("b");

        dijkstra<std::string, int> searcher{g, a};

        REQUIRE(!searcher.search_path(b).has_value());
    }

    SECTION("Example CLRS")
    {
        graph<std::string, int> original_graph;

        int z = original_graph.add_vertex("Z");
        int x = original_graph.add_vertex("X");
        int y = original_graph.add_vertex("Y");
        int t = original_graph.add_vertex("T");
        int s = original_graph.add_vertex("S");

        original_graph.add_edge(s, t, 10);
        original_graph.add_edge(s, y, 5);
        original_graph.add_edge(t, y, 2);
        original_graph.add_edge(t, x, 1);
        original_graph.add_edge(y, t, 3);
        original_graph.add_edge(y, x, 9);
        original_graph.add_edge(y, z, 2);
        original_graph.add_edge(x, z, 4);
        original_graph.add_edge(z, s, 7);
        original_graph.add_edge(z, x, 6);

        dijkstra<std::string, int> searcher{original_graph, s};

        auto result = searcher.search_path(s);
        REQUIRE(result.has_value());
        REQUIRE(result.value().first == std::vector<std::ptrdiff_t>{s});
        REQUIRE(result.value().second == 0);

        result = searcher.search_path(y);
        REQUIRE(result.has_value());
        REQUIRE(result.value().first == std::vector<std::ptrdiff_t>{s, y});
        REQUIRE(result.value().second == 5);

        result = searcher.search_path(t);
        REQUIRE(result.has_value());
        REQUIRE(result.value().first == std::vector<std::ptrdiff_t>{s, y, t});
        REQUIRE(result.value().second == 8);

        result = searcher.search_path(x);
        REQUIRE(result.has_value());
        REQUIRE(result.value().first == std::vector<std::ptrdiff_t>{s, y, t, x});
        REQUIRE(result.value().second == 9);

        result = searcher.search_path(z);
        REQUIRE(result.has_value());
        REQUIRE(result.value().first == std::vector<std::ptrdiff_t>{s, y, z});
        REQUIRE(result.value().second == 7);
    }
}