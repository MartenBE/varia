// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "catch2/catch_template_test_macros.hpp"
#include "knuth-morris-pratt.hpp"
#include "naieve-string-search.hpp"

#include <vector>

struct test_data_type
{
    test_data_type(std::string text, std::string pattern, std::vector<std::ptrdiff_t> expected_results)
    : m_text{text}, m_pattern{pattern}, m_expected_results{expected_results}
    {
    }

    std::string m_text;
    std::string m_pattern;
    std::vector<std::ptrdiff_t> m_expected_results;
};

std::vector<test_data_type> test_data{{"", "abc", {}},
                                      {"xxxxxxx", "abc", {}},
                                      {"xxabcxx", "abc", {2}},
                                      {"abcxxxx", "abc", {0}},
                                      {"xxxxabc", "abc", {4}},
                                      {"abacadabrac", "ab", {0, 6}},
                                      {"ababa", "aba", {0, 2}},
                                      {"abababab", "abab", {0, 2, 4}},
                                      {"aaa", "a", {0, 1, 2}},
                                      {"aaaaa", "aaa", {0, 1, 2}},
                                      {"abcabcdababcdabcdabde", "abcdabd", {13}}};

TEMPLATE_TEST_CASE("Search string", "", (naieve_string_search))
{
    for (const auto& td : test_data)
    {
        auto results = TestType::search(td.m_text, td.m_pattern);
        REQUIRE(results == td.m_expected_results);
    }
}

TEMPLATE_TEST_CASE("Search string by preprocessing the search pattern", "", (knuth_morris_pratt))
{
    for (const auto& td : test_data)
    {
        TestType searcher{td.m_pattern};
        auto results = searcher.search(td.m_text);
        REQUIRE(results == td.m_expected_results);
    }
}