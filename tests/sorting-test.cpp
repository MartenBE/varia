// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "bubblesort.hpp"
#include "catch2/catch_template_test_macros.hpp"
#include "countingsort.hpp"
#include "heapsort.hpp"
#include "insertionsort.hpp"
#include "mergesort.hpp"
#include "quicksort.hpp"
#include "selectionsort.hpp"

#include <algorithm>
#include <array>
#include <numeric>
#include <random>
#include <vector>

TEMPLATE_TEST_CASE("Sort vectors",
                   "",
                   (selectionsort<int>),
                   (insertionsort<int>),
                   (bubblesort<int>),
                   (quicksort<int>),
                   (heapsort<int>),
                   (mergesort<int>),
                   (countingsort<int>) )
{
    TestType sorter;

    std::random_device rd;
    std::mt19937 eng{rd()};
    constexpr std::array<int, 6> amount_test_data = {4, 8, 10, 256, 1000, 1234};

    std::vector<int> no_numbers = {};
    std::vector<int> one_number = {0};
    std::vector<int> sorted_numbers = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::vector<int> sorted_numbers_with_equals = {0, 2, 2, 3, 3, 4, 4, 7, 8, 9};
    std::vector<int> inverse_sorted_numbers = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
    std::vector<int> inverse_sorted_numbers_with_equals = {9, 8, 7, 4, 4, 3, 3, 2, 2, 0};
    std::vector<int> unsorted_numbers = {3, 0, 8, 6, 7, 2, 9, 1, 4, 5};
    std::vector<int> unsorted_numbers_with_equals = {8, 4, 4, 0, 9, 2, 7, 2, 3, 3};
    std::vector<int> equal_numbers = {7, 7, 7, 7, 7, 7, 7, 7, 7, 7};

    SECTION("Sort empty list (hardcoded elements)")
    {
        std::vector<int> original_test_values(no_numbers);

        sorter(no_numbers.begin(), no_numbers.end());

        REQUIRE(std::is_permutation(no_numbers.begin(), no_numbers.end(), original_test_values.begin()));
        REQUIRE(std::is_sorted(no_numbers.begin(), no_numbers.end()));
    }

    SECTION("Sort list with 1 element (hardcoded elements)")
    {
        sorter(one_number.begin(), one_number.end());

        REQUIRE(std::is_sorted(one_number.begin(), one_number.end()));
    }

    SECTION("Sort already sorted list without equals (hardcoded elements)")
    {
        sorter(sorted_numbers.begin(), sorted_numbers.end());

        REQUIRE(std::is_sorted(sorted_numbers.begin(), sorted_numbers.end()));
    }

    SECTION("Sort already sorted list with equals (hardcoded elements)")
    {
        sorter(sorted_numbers_with_equals.begin(), sorted_numbers_with_equals.end());

        REQUIRE(std::is_sorted(sorted_numbers_with_equals.begin(), sorted_numbers_with_equals.end()));
    }

    SECTION("Sort already reversed sorted list without equals (hardcoded elements)")
    {
        sorter(inverse_sorted_numbers.begin(), inverse_sorted_numbers.end());

        REQUIRE(std::is_sorted(inverse_sorted_numbers.begin(), inverse_sorted_numbers.end()));
    }

    SECTION("Sort already reversed sorted list with equals (hardcoded elements)")
    {
        sorter(inverse_sorted_numbers_with_equals.begin(), inverse_sorted_numbers_with_equals.end());

        REQUIRE(std::is_sorted(inverse_sorted_numbers_with_equals.begin(), inverse_sorted_numbers_with_equals.end()));
    }

    SECTION("Sort random list without equals (hardcoded elements)")
    {
        sorter(unsorted_numbers.begin(), unsorted_numbers.end());

        REQUIRE(std::is_sorted(unsorted_numbers.begin(), unsorted_numbers.end()));
    }

    SECTION("Sort random list with equals (hardcoded elements)")
    {
        sorter(unsorted_numbers_with_equals.begin(), unsorted_numbers_with_equals.end());

        REQUIRE(std::is_sorted(unsorted_numbers_with_equals.begin(), unsorted_numbers_with_equals.end()));
    }

    SECTION("Sort random list with equals only (hardcoded elements)")
    {
        sorter(equal_numbers.begin(), equal_numbers.end());

        REQUIRE(std::is_sorted(equal_numbers.begin(), equal_numbers.end()));
    }

    SECTION("Check for bugfix quicksort (error finding correct bound)")
    {
        // Forgot left++ and right-- while loops, after pointers crossed

        std::vector<std::vector<int>> test_values = {{1, 0, 2, 1}, {1, 3, 0, 4, 3}, {2, 3, 4, 2, 4}};

        for (auto& tv : test_values)
        {
            sorter(tv.begin(), tv.end());

            REQUIRE(std::is_sorted(tv.begin(), tv.end()));
        }
    }

    // TODO move to fuzzy testing
    for (const auto& amount : amount_test_data)
    {
        SECTION("Sort already sorted list")
        {
            std::vector<int> test(amount);

            std::iota(test.begin(), test.end(), 1);

            sorter(test.begin(), test.end());

            REQUIRE(std::is_sorted(test.begin(), test.end()));
        }

        SECTION("Sort already reversed sorted list")
        {
            std::vector<int> test(amount);

            std::iota(test.begin(), test.end(), 1);
            std::reverse(test.begin(), test.end());

            sorter(test.begin(), test.end());

            REQUIRE(std::is_sorted(test.begin(), test.end()));
        }

        SECTION("Sort random list without equals")
        {
            std::vector<int> test(amount);

            std::iota(test.begin(), test.end(), 1);
            std::shuffle(test.begin(), test.end(), eng);

            sorter(test.begin(), test.end());

            REQUIRE(std::is_sorted(test.begin(), test.end()));
        }

        SECTION("Sort random list with equals")
        {
            std::vector<int> test(amount);

            const auto max_value = (test.size() - 1);
            assert(max_value < std::numeric_limits<int>::max());
            std::uniform_int_distribution<int> dist{0, static_cast<int>(max_value)};

            std::generate(test.begin(), test.end(), [&dist, &rd]() { return dist(rd); });

            std::vector<int> original_test(test);

            sorter(test.begin(), test.end());

            REQUIRE(std::is_permutation(test.begin(), test.end(), original_test.begin()));
            REQUIRE(std::is_sorted(test.begin(), test.end()));
        }
    }
}