// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "catch2/catch_test_macros.hpp"
#include "combinatorics.hpp"

TEST_CASE("Calculate permutations")
{
    REQUIRE(permutation(3) == 6);
    REQUIRE(permutation(12) == 479'001'600);
}

TEST_CASE("Calculate variations")
{
    REQUIRE(variation(10, 0) == 1);
    REQUIRE(variation(10, 10) == permutation(10));

    REQUIRE(variation(12, 3) == 1320);
    REQUIRE(variation(5, 3) == 60);
}

TEST_CASE("Calculate variations with repetition")
{
    REQUIRE(variation_repetition(2, 8) == 256);
    REQUIRE(variation_repetition(5, 2) == 25);
}

TEST_CASE("Calculate combinations")
{
    REQUIRE(combination(12, 2) == 66);
    REQUIRE(combination(5, 3) == 10);
}

TEST_CASE("Calculate combinations with repetition")
{
    REQUIRE(combination_repetition(5, 8) == 495);
    REQUIRE(combination_repetition(4, 6) == 84);
}