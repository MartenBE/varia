// Copyright (C) 2022 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "catch2/catch_test_macros.hpp"
#include "longest-common-substring.hpp"

// TEST_CASE("Find LCS between 2 strings with multiple possible solutions (wikipedia)")
// {
//     REQUIRE(longest_common_substring::find("ABCD", "ACBAD") == "abc");
//     REQUIRE(longest_common_substring::find("GAC", "AGCAT") == "GA");
// }

TEST_CASE("Find LCS between 2 strings (CLRS)")
{
    REQUIRE(longest_common_substring::find("ACCGGTCGAGTGCGCGGAAGCCGGCCGAA", "GTCGTTCGGAATGCCGTTGCTCTGTAAA")
            == "GTCGTCGGAAGCCGGCCGAA");
}

TEST_CASE("Find LCS between 2 strings of which one is empty")
{
    REQUIRE(longest_common_substring::find("qwerty", "") == "");
    REQUIRE(longest_common_substring::find("", "qwerty") == "");
}

TEST_CASE("Find LCS between 2 empty strings")
{
    REQUIRE(longest_common_substring::find("", "") == "");
}