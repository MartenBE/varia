// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "catch2/catch_template_test_macros.hpp"
#include "sorted-array-search.hpp"

#include <array>
#include <string>
#include <utility>

struct linear_search_wrapper
{
    template <class random_access_iterator, class T>
    static std::optional<random_access_iterator>
    invoke(random_access_iterator begin, random_access_iterator end, const T& value)
    {
        return sorted_array_search::linear_search(begin, end, value);
    }
};

struct binary_search_iterative_wrapper
{
    template <class random_access_iterator, class T>
    static std::optional<random_access_iterator>
    invoke(random_access_iterator begin, random_access_iterator end, const T& value)
    {
        return sorted_array_search::binary_search_iterative(begin, end, value);
    }
};

struct binary_search_recursive_wrapper
{
    template <class random_access_iterator, class T>
    static std::optional<random_access_iterator>
    invoke(random_access_iterator begin, random_access_iterator end, const T& value)
    {
        return sorted_array_search::binary_search_recursive(begin, end, value);
    }
};

TEMPLATE_TEST_CASE("Search in an sorted array",
                   "",
                   (linear_search_wrapper),
                   (binary_search_iterative_wrapper),
                   (binary_search_recursive_wrapper))
{
    std::array<int, 0> test_empty;
    std::array<int, 6> test_even = {0, 10, 20, 30, 40, 50};
    std::array<int, 7> test_odd = {0, 10, 20, 30, 40, 50, 60};

    SECTION("Search in empty array")
    {
        auto result = TestType::invoke(test_empty.begin(), test_empty.end(), 1337);

        REQUIRE(!result.has_value());
    }

    SECTION("Search in array with even amount of elements")
    {
        for (std::ptrdiff_t i = 0; i < std::ssize(test_even); i++)
        {
            int expected_value = (i * 10);
            auto result = TestType::invoke(test_even.begin(), test_even.end(), expected_value);

            REQUIRE(result.has_value());
            REQUIRE(*(result.value()) == expected_value);
        }
    }

    SECTION("Search in array with odd amount of elements")
    {
        for (std::ptrdiff_t i = 0; i < std::ssize(test_odd); i++)
        {
            int expected_value = (i * 10);
            auto result = TestType::invoke(test_odd.begin(), test_odd.end(), expected_value);

            REQUIRE(result.has_value());
            REQUIRE(*(result.value()) == expected_value);
        }
    }

    SECTION("Search in array without result")
    {
        auto result = TestType::invoke(test_odd.begin(), test_odd.end(), 1337);

        REQUIRE(!result.has_value());

        result = TestType::invoke(test_odd.begin(), test_odd.end(), 35);

        REQUIRE(!result.has_value());

        result = TestType::invoke(test_odd.begin(), test_odd.end(), -1337);

        REQUIRE(!result.has_value());
    }
}