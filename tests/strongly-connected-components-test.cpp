// Copyright (C) 2021 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "catch2/catch_template_test_macros.hpp"
#include "strongly-connected-components.hpp"

template <class V, class E>
struct kosaraju_wrapper
{
    static std::set<std::set<std::ptrdiff_t>> invoke(const graph<V, E>& graph)
    {
        return strongly_connected_components<V, E>::kosaraju(graph);
    }
};

// template <class V, class E>
// struct tarjan_wrapper
// {
//     static std::set<std::set<std::ptrdiff_t>> invoke(const graph<V, E>& graph)
//     {
//         return strongly_connected_components<V, E>::tarjan(graph);
//     }
// };

TEMPLATE_TEST_CASE("Search strongly connected components in graph",
                   "",
                   (kosaraju_wrapper<std::string, int>)/*,
                   (tarjan_wrapper<std::string, int>)*/ )
{
    SECTION("Simple example")
    {
        graph<std::string, int> gr;

        std::ptrdiff_t a = gr.add_vertex("a");
        std::ptrdiff_t b = gr.add_vertex("b");
        std::ptrdiff_t c = gr.add_vertex("c");
        std::ptrdiff_t d = gr.add_vertex("d");

        gr.add_edge(a, b, 1);
        gr.add_edge(a, c, 1);
        gr.add_edge(b, a, 1);
        gr.add_edge(c, d, 1);
        gr.add_edge(d, c, 1);

        std::set<std::set<std::ptrdiff_t>> expected_components = {{a, b}, {c, d}};

        REQUIRE(TestType::invoke(gr) == expected_components);
    }

    SECTION("Example stackoverflow why you need to reverse the graph in Kosaraju's algorithm ( e_noether at https://cs.stackexchange.com/questions/48625/kosarajus-algorithm-strongly-connected-components )")
    {
        graph<std::string, int> gr;

        std::ptrdiff_t a = gr.add_vertex("a");
        std::ptrdiff_t b = gr.add_vertex("b");
        std::ptrdiff_t c = gr.add_vertex("c");

        gr.add_edge(a, b, 1);
        gr.add_edge(a, c, 1);
        gr.add_edge(b, a, 1);

        std::set<std::set<std::ptrdiff_t>> expected_components = {{b, a}, {c}};

        REQUIRE(TestType::invoke(gr) == expected_components);
    }

    SECTION("Example Wikipedia")
    {
        graph<std::string, int> gr;

        std::ptrdiff_t a = gr.add_vertex("a");
        std::ptrdiff_t b = gr.add_vertex("b");
        std::ptrdiff_t c = gr.add_vertex("c");
        std::ptrdiff_t d = gr.add_vertex("d");
        std::ptrdiff_t e = gr.add_vertex("e");
        std::ptrdiff_t f = gr.add_vertex("f");
        std::ptrdiff_t g = gr.add_vertex("g");
        std::ptrdiff_t h = gr.add_vertex("h");

        gr.add_edge(a, b, 1);
        gr.add_edge(b, e, 1);
        gr.add_edge(b, f, 1);
        gr.add_edge(b, c, 1);
        gr.add_edge(c, d, 1);
        gr.add_edge(c, g, 1);
        gr.add_edge(d, c, 1);
        gr.add_edge(d, h, 1);
        gr.add_edge(e, a, 1);
        gr.add_edge(e, f, 1);
        gr.add_edge(f, g, 1);
        gr.add_edge(g, f, 1);
        gr.add_edge(h, g, 1);
        gr.add_edge(h, d, 1);

        std::set<std::set<std::ptrdiff_t>> expected_components = {{a, b, e}, {c, d, h}, {f, g}};

        REQUIRE(TestType::invoke(gr) == expected_components);
    }

    SECTION("Example Sedgewick")
    {
        graph<std::string, int> gr;

        std::ptrdiff_t v0 = gr.add_vertex("0");
        std::ptrdiff_t v1 = gr.add_vertex("1");
        std::ptrdiff_t v2 = gr.add_vertex("2");
        std::ptrdiff_t v3 = gr.add_vertex("3");
        std::ptrdiff_t v4 = gr.add_vertex("4");
        std::ptrdiff_t v5 = gr.add_vertex("5");
        std::ptrdiff_t v6 = gr.add_vertex("6");
        std::ptrdiff_t v7 = gr.add_vertex("7");
        std::ptrdiff_t v8 = gr.add_vertex("8");
        std::ptrdiff_t v9 = gr.add_vertex("9");
        std::ptrdiff_t v10 = gr.add_vertex("10");
        std::ptrdiff_t v11 = gr.add_vertex("11");
        std::ptrdiff_t v12 = gr.add_vertex("12");

        gr.add_edge(v0, v1, 1);
        gr.add_edge(v0, v5, 1);
        gr.add_edge(v2, v0, 1);
        gr.add_edge(v2, v3, 1);
        gr.add_edge(v3, v2, 1);
        gr.add_edge(v3, v5, 1);
        gr.add_edge(v4, v2, 1);
        gr.add_edge(v4, v3, 1);
        gr.add_edge(v5, v4, 1);
        gr.add_edge(v6, v0, 1);
        gr.add_edge(v6, v4, 1);
        gr.add_edge(v6, v8, 1);
        gr.add_edge(v6, v9, 1);
        gr.add_edge(v7, v6, 1);
        gr.add_edge(v7, v9, 1);
        gr.add_edge(v8, v6, 1);
        gr.add_edge(v9, v10, 1);
        gr.add_edge(v9, v11, 1);
        gr.add_edge(v10, v12, 1);
        gr.add_edge(v11, v4, 1);
        gr.add_edge(v11, v12, 1);
        gr.add_edge(v12, v9, 1);

        std::set<std::set<std::ptrdiff_t>> expected_components = {
                {v1}, {v0, v2, v3, v4, v5}, {v6, v8}, {v7}, {v9, v10, v11, v12}};

        REQUIRE(TestType::invoke(gr) == expected_components);
    }

    SECTION("Example CLRS")
    {
        graph<std::string, int> gr;

        std::ptrdiff_t a = gr.add_vertex("a");
        std::ptrdiff_t b = gr.add_vertex("b");
        std::ptrdiff_t c = gr.add_vertex("c");
        std::ptrdiff_t d = gr.add_vertex("d");
        std::ptrdiff_t e = gr.add_vertex("e");
        std::ptrdiff_t f = gr.add_vertex("f");
        std::ptrdiff_t g = gr.add_vertex("g");
        std::ptrdiff_t h = gr.add_vertex("h");

        gr.add_edge(a, b, 1);
        gr.add_edge(b, e, 1);
        gr.add_edge(b, f, 1);
        gr.add_edge(b, c, 1);
        gr.add_edge(c, d, 1);
        gr.add_edge(c, g, 1);
        gr.add_edge(d, c, 1);
        gr.add_edge(d, h, 1);
        gr.add_edge(e, a, 1);
        gr.add_edge(e, f, 1);
        gr.add_edge(f, g, 1);
        gr.add_edge(g, f, 1);
        gr.add_edge(g, h, 1);
        gr.add_edge(h, h, 1);

        std::set<std::set<std::ptrdiff_t>> expected_components = {{a, b, e}, {c, d}, {f, g}, {h}};

        REQUIRE(TestType::invoke(gr) == expected_components);
    }
}