// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "catch2/catch_template_test_macros.hpp"
#include "topological-sort.hpp"

template <class V, class E>
struct depth_first_search_wrapper
{
    static std::vector<std::ptrdiff_t> invoke(const graph<V, E>& graph)
    {
        return topological_sort<V, E>::depth_first_search(graph);
    }
};

template <class V, class E>
struct kahn_iterative_wrapper
{
    static std::vector<std::ptrdiff_t> invoke(const graph<V, E>& graph)
    {
        return topological_sort<V, E>::kahn(graph);
    }
};

TEMPLATE_TEST_CASE("Determine the topological sort of a graph",
                   "",
                   (depth_first_search_wrapper<std::string, int>),
                   (kahn_iterative_wrapper<std::string, int>) )
{
    SECTION("Empty graph")
    {
        graph<std::string, int> gr;

        std::vector<std::ptrdiff_t> result = TestType::invoke(gr);

        REQUIRE(result == std::vector<std::ptrdiff_t>{});
    }

    SECTION("Example Skiena")
    {
        graph<std::string, int> gr;

        std::ptrdiff_t a = gr.add_vertex("a");
        std::ptrdiff_t b = gr.add_vertex("b");
        std::ptrdiff_t c = gr.add_vertex("c");
        std::ptrdiff_t d = gr.add_vertex("d");
        std::ptrdiff_t e = gr.add_vertex("e");
        std::ptrdiff_t f = gr.add_vertex("f");
        std::ptrdiff_t g = gr.add_vertex("g");

        gr.add_edge(a, b, 1);
        gr.add_edge(a, c, 1);
        gr.add_edge(b, c, 1);
        gr.add_edge(b, d, 1);
        gr.add_edge(c, e, 1);
        gr.add_edge(c, f, 1);
        gr.add_edge(e, d, 1);
        gr.add_edge(f, e, 1);
        gr.add_edge(g, a, 1);
        gr.add_edge(g, f, 1);

        std::vector<std::ptrdiff_t> result = TestType::invoke(gr);

        REQUIRE(result == std::vector<std::ptrdiff_t>{g, a, b, c, f, e, d});
    }
}