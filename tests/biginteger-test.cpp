// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "catch2/catch_test_macros.hpp"
#include "dumb-biginteger.hpp"

#include <sstream>

TEST_CASE("Create a big integer using default constructor")
{
    dumb_biginteger zero_bi;

    std::stringstream out;
    out << zero_bi;
    REQUIRE(out.str() == "0");
}

TEST_CASE("Create a big integer using integer")
{
    dumb_biginteger pos_bi(9223372036854775807LL);

    std::stringstream out;
    out << pos_bi;
    REQUIRE(out.str() == "9223372036854775807");
}

TEST_CASE("Create a big integer using strings")
{
    dumb_biginteger pos_bi{"1234567890123465789"};

    std::stringstream out;
    out << pos_bi;
    REQUIRE(out.str() == "1234567890123465789");
}

TEST_CASE("Create a big integer with leading zeroes using strings")
{
    dumb_biginteger pos_bi{"00000001234567890123465789"};

    std::stringstream out;
    out << pos_bi;
    REQUIRE(out.str() == "1234567890123465789");
}

TEST_CASE("Create a big integer with leading plus using strings")
{
    dumb_biginteger pos_bi{"+1234567890123465789"};

    std::stringstream out;
    out << pos_bi;
    REQUIRE(out.str() == "1234567890123465789");
}

TEST_CASE("Create a big integer with leading plus and leading zeroes using strings")
{
    dumb_biginteger pos_bi{"+00000001234567890123465789"};

    std::stringstream out;
    out << pos_bi;
    REQUIRE(out.str() == "1234567890123465789");
}

TEST_CASE("Create a big integer with leading minus using strings")
{
    dumb_biginteger pos_bi{"-1234567890123465789"};

    std::stringstream out;
    out << pos_bi;
    REQUIRE(out.str() == "-1234567890123465789");
}

TEST_CASE("Create a big integer with leading minus and leading zeroes using strings")
{
    dumb_biginteger pos_bi{"-00000001234567890123465789"};

    std::stringstream out;
    out << pos_bi;
    REQUIRE(out.str() == "-1234567890123465789");
}

TEST_CASE("Create a big integer of zeroes only using strings")
{
    dumb_biginteger pos_bi{"0000000"};

    std::stringstream out;
    out << pos_bi;
    REQUIRE(out.str() == "0");
}

TEST_CASE("Add two positive big integers")
{
    dumb_biginteger bi_a{"123456789012345678901234567890"};
    dumb_biginteger bi_b{"12345678901234567890"};

    REQUIRE((bi_a + bi_b) == dumb_biginteger{"123456789024691357802469135780"});
    REQUIRE((bi_b + bi_a) == dumb_biginteger{"123456789024691357802469135780"});

    bi_a = dumb_biginteger{"999999999999"};
    bi_b = dumb_biginteger{"1"};

    REQUIRE((bi_a + bi_a) == dumb_biginteger{"1999999999998"});
    REQUIRE((bi_a + bi_b) == dumb_biginteger{"1000000000000"});
}

TEST_CASE("Add two negative big integers")
{
    dumb_biginteger bi_a{"-123456789012345678901234567890"};
    dumb_biginteger bi_b{"-12345678901234567890"};

    REQUIRE((bi_a + bi_b) == dumb_biginteger{"-123456789024691357802469135780"});
    REQUIRE((bi_b + bi_a) == dumb_biginteger{"-123456789024691357802469135780"});

    bi_a = dumb_biginteger{"-999999999999"};
    bi_b = dumb_biginteger{"-1"};

    REQUIRE((bi_a + bi_a) == dumb_biginteger{"-1999999999998"});
    REQUIRE((bi_a + bi_b) == dumb_biginteger{"-1000000000000"});
}

TEST_CASE("Add a positive and a negative big integers")
{
    dumb_biginteger bi_a{"-123456789"};
    dumb_biginteger bi_b{"987654321"};

    REQUIRE((bi_a + bi_b) == dumb_biginteger{"864197532"});
    REQUIRE((bi_b + bi_a) == dumb_biginteger{"864197532"});

    bi_a = dumb_biginteger{"123456789"};
    bi_b = dumb_biginteger{"-987654321"};

    REQUIRE((bi_a + bi_b) == dumb_biginteger{"-864197532"});
    REQUIRE((bi_b + bi_a) == dumb_biginteger{"-864197532"});
}

TEST_CASE("Adding the inverse")
{
    dumb_biginteger bi_a{"1337"};
    dumb_biginteger bi_b{"-1337"};

    REQUIRE((bi_a + bi_b) == dumb_biginteger{"0"});
}

TEST_CASE("Additive Identity Property")
{
    dumb_biginteger bi_a{"1337"};
    dumb_biginteger bi_b{"0"};

    REQUIRE((bi_a + bi_b) == dumb_biginteger{"1337"});
}

TEST_CASE("Subtract two positive big integers")
{
    dumb_biginteger bi_a{"123456789012345678901234567890"};
    dumb_biginteger bi_b{"12345678901234567890"};

    REQUIRE((bi_a - bi_b) == dumb_biginteger{"123456789000000000000000000000"});
    REQUIRE((bi_b - bi_a) == dumb_biginteger{"-123456789000000000000000000000"});

    bi_a = dumb_biginteger{"999999999999"};
    bi_b = dumb_biginteger{"1"};

    REQUIRE((bi_a - bi_b) == dumb_biginteger{"999999999998"});
    REQUIRE((bi_b - bi_a) == dumb_biginteger{"-999999999998"});
}

TEST_CASE("Subtract two negative big integers")
{
    dumb_biginteger bi_a{"-123456789012345678901234567890"};
    dumb_biginteger bi_b{"-12345678901234567890"};

    REQUIRE((bi_a - bi_b) == dumb_biginteger{"-123456789000000000000000000000"});
    REQUIRE((bi_b - bi_a) == dumb_biginteger{"123456789000000000000000000000"});

    bi_a = dumb_biginteger{"-999999999999"};
    bi_b = dumb_biginteger{"-1"};

    REQUIRE((bi_a - bi_b) == dumb_biginteger{"-999999999998"});
    REQUIRE((bi_a - bi_b) == dumb_biginteger{"-999999999998"});
}

TEST_CASE("Subtract a positive and a negative big integers")
{
    dumb_biginteger bi_a{"-123456789"};
    dumb_biginteger bi_b{"987654321"};

    REQUIRE((bi_a - bi_b) == dumb_biginteger{"-1111111110"});
    REQUIRE((bi_b - bi_a) == dumb_biginteger{"1111111110"});

    bi_a = dumb_biginteger{"123456789"};
    bi_b = dumb_biginteger{"-987654321"};

    REQUIRE((bi_a - bi_b) == dumb_biginteger{"1111111110"});
    REQUIRE((bi_b - bi_a) == dumb_biginteger{"-1111111110"});
}

TEST_CASE("Subtracting with zeros")
{
    REQUIRE((dumb_biginteger{"50"} - dumb_biginteger{"23"}) == dumb_biginteger{"27"});
    REQUIRE((dumb_biginteger{"60"} - dumb_biginteger{"50"}) == dumb_biginteger{"10"});
    REQUIRE((dumb_biginteger{"900"} - dumb_biginteger{"36"}) == dumb_biginteger{"864"});
}