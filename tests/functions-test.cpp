// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "catch2/catch_test_macros.hpp"
#include "simple-functions.hpp"

TEST_CASE("Calculate factorial")
{
    REQUIRE(simple_functions::factorial(0) == 1);
    REQUIRE(simple_functions::factorial(1) == 1);
    REQUIRE(simple_functions::factorial(2) == 2);
    REQUIRE(simple_functions::factorial(6) == 720);
    REQUIRE(simple_functions::factorial(10) == 3628800);
}

TEST_CASE("Find prime numbers using sieve of Eratosthenes")
{
    REQUIRE(simple_functions::sieve_eratosthenes(10) == std::vector<int>{2, 3, 5, 7});
    REQUIRE(simple_functions::sieve_eratosthenes(120) == std::vector<int>{2,  3,  5,  7,  11, 13,  17,  19,  23,  29,
                                                                          31, 37, 41, 43, 47, 53,  59,  61,  67,  71,
                                                                          73, 79, 83, 89, 97, 101, 103, 107, 109, 113});
}

TEST_CASE("Find the greatest common divisor with the euclidean algorithm")
{
    REQUIRE(simple_functions::euclidean_algorithm(1470, 825) == 15);
    REQUIRE(simple_functions::euclidean_algorithm(105, 30) == 15);
    REQUIRE(simple_functions::euclidean_algorithm(30, 15) == 15);
    REQUIRE(simple_functions::euclidean_algorithm(15, 0) == 15);
    REQUIRE(simple_functions::euclidean_algorithm(252, 105) == 21);
}

TEST_CASE("Calculate amount digits")
{
    REQUIRE(simple_functions::amount_digits(0) == 1);
    REQUIRE(simple_functions::amount_digits(1) == 1);
    REQUIRE(simple_functions::amount_digits(9) == 1);
    REQUIRE(simple_functions::amount_digits(10) == 2);
    REQUIRE(simple_functions::amount_digits(11) == 2);
    REQUIRE(simple_functions::amount_digits(999) == 3);
    REQUIRE(simple_functions::amount_digits(1000) == 4);
    REQUIRE(simple_functions::amount_digits(1001) == 4);
    REQUIRE(simple_functions::amount_digits(5000) == 4);
    REQUIRE(simple_functions::amount_digits(9999) == 4);
    REQUIRE(simple_functions::amount_digits(10000) == 5);
    REQUIRE(simple_functions::amount_digits(9223372036854775807) == 19);
}