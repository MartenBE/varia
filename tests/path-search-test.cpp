// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "catch2/catch_template_test_macros.hpp"
#include "path-search.hpp"

#include <map>
#include <vector>

template <class V, class E>
struct depth_first_search_recursive_wrapper
{
    static std::optional<std::vector<std::ptrdiff_t>>
    invoke(const graph<V, E>& graph, std::ptrdiff_t start_vertex_id, std::ptrdiff_t target_vertex_id)
    {
        return path_search<V, E>::depth_first_search_recursive(graph, start_vertex_id, target_vertex_id);
    }
};

template <class V, class E>
struct depth_first_search_iterative_wrapper
{
    static std::optional<std::vector<std::ptrdiff_t>>
    invoke(const graph<V, E>& graph, std::ptrdiff_t start_vertex_id, std::ptrdiff_t target_vertex_id)
    {
        return path_search<V, E>::depth_first_search_iterative(graph, start_vertex_id, target_vertex_id);
    }
};

template <class V, class E>
struct breadth_first_search_wrapper
{
    static std::optional<std::vector<std::ptrdiff_t>>
    invoke(const graph<V, E>& graph, std::ptrdiff_t start_vertex_id, std::ptrdiff_t target_vertex_id)
    {
        return path_search<V, E>::breadth_first_search(graph, start_vertex_id, target_vertex_id);
    }
};

TEMPLATE_TEST_CASE("Search paths in graph",
                   "",
                   (depth_first_search_recursive_wrapper<std::string, int>),
                   (depth_first_search_iterative_wrapper<std::string, int>),
                   (breadth_first_search_wrapper<std::string, int>) )
{
    SECTION("Path construction")
    {
        graph<std::string, int> g;

        int a = g.add_vertex("a");
        int b = g.add_vertex("b");
        int c = g.add_vertex("c");
        int d = g.add_vertex("d");
        int e = g.add_vertex("e");

        g.add_undirected_edge(a, b, 1);
        g.add_undirected_edge(b, c, 1);
        g.add_undirected_edge(c, d, 1);
        g.add_undirected_edge(d, e, 1);

        auto result = TestType::invoke(g, a, e);
        REQUIRE(result.has_value());
        REQUIRE(result.value() == std::vector<std::ptrdiff_t>{a, b, c, d, e});
    }

    SECTION("Path to itself")
    {
        graph<std::string, int> g;

        int a = g.add_vertex("a");

        auto result = TestType::invoke(g, a, a);
        REQUIRE(result.has_value());
        REQUIRE(std::ssize(result.value()) == 1);
    }

    SECTION("No direct path")
    {
        graph<std::string, int> g;

        int a = g.add_vertex("a");
        int b = g.add_vertex("b");

        REQUIRE(!TestType::invoke(g, a, b).has_value());
    }
}

TEST_CASE("Example CLRS")
{
    graph<std::string, int> g;

    int r = g.add_vertex("r");
    int s = g.add_vertex("s");
    int t = g.add_vertex("t");
    int u = g.add_vertex("u");
    int v = g.add_vertex("v");
    int w = g.add_vertex("w");
    int x = g.add_vertex("x");
    int y = g.add_vertex("y");

    g.add_undirected_edge(r, v, 1);
    g.add_undirected_edge(r, s, 1);
    g.add_undirected_edge(s, w, 1);
    g.add_undirected_edge(w, t, 1);
    g.add_undirected_edge(w, x, 1);
    g.add_undirected_edge(t, x, 1);
    g.add_undirected_edge(t, u, 1);
    g.add_undirected_edge(x, u, 1);
    g.add_undirected_edge(x, y, 1);
    g.add_undirected_edge(u, y, 1);

    std::vector<std::ptrdiff_t> vertex_ids = {r, s, t, u, v, w, x, y};

    std::map<std::pair<std::ptrdiff_t, std::ptrdiff_t>, int> reachable_paths = {
            {{s, s}, 1},
            {{s, r}, 2},
            {{s, w}, 2},
            {{s, v}, 3},
            {{s, t}, 3},
            {{s, x}, 3},
            {{s, u}, 4},
            {{s, y}, 4},
    };

    for (const auto& target_vertex_id : vertex_ids)
    {
        auto result = path_search<std::string, int>::breadth_first_search(g, s, target_vertex_id);

        auto it = reachable_paths.find({s, target_vertex_id});
        assert(it != reachable_paths.end());

        REQUIRE(result.has_value());
        REQUIRE(std::ssize(result.value()) == it->second);
    }
}