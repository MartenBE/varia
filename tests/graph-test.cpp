// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "catch2/catch_test_macros.hpp"
#include "graph.hpp"

#include <algorithm>
#include <regex>
#include <sstream>

TEST_CASE("Create a directed data graph")
{
    graph<std::string, std::string> g;

    std::ptrdiff_t a = g.add_vertex("a");
    std::ptrdiff_t b = g.add_vertex("b");
    std::ptrdiff_t c = g.add_vertex("c");
    std::ptrdiff_t d = g.add_vertex("d");

    REQUIRE(a == 0);
    REQUIRE(b == 1);
    REQUIRE(c == 2);
    REQUIRE(d == 3);

    std::ptrdiff_t ab = g.add_edge(a, b, "a->b");
    std::ptrdiff_t bc = g.add_edge(b, c, "b->c");
    std::ptrdiff_t cd = g.add_edge(c, d, "c->d");
    std::ptrdiff_t da = g.add_edge(d, a, "d->a");
    std::ptrdiff_t bd = g.add_edge(b, d, "b->d");
    std::ptrdiff_t db = g.add_edge(d, b, "d->b");

    REQUIRE(ab == 0);
    REQUIRE(bc == 1);
    REQUIRE(cd == 2);
    REQUIRE(da == 3);
    REQUIRE(bd == 4);
    REQUIRE(db == 5);

    SECTION("Copy constructor")
    {
        graph<std::string, std::string> copy{g};
        REQUIRE(g.to_json() == copy.to_json());
    }

    SECTION("Assignment operator")
    {
        graph<std::string, std::string> copy = g;
        REQUIRE(g.to_json() == copy.to_json());
    }

    SECTION("Add vertex")
    {
        REQUIRE(g.add_vertex("test_vertex") == 4);
    }

    SECTION("Get vertex data")
    {
        REQUIRE(g.vertex_data(a) == "a");
    }

    SECTION("Remove vertex")
    {
        g.remove_vertex(b);

        auto expected_json = nlohmann::json::parse(R"(
            {
                "graph": 
                {
                    "edges": 
                    [
                        {
                            "data": "c->d",
                            "id": 2,
                            "source": 2,
                            "target": 3
                        },
                        {
                            "data": "d->a",
                            "id": 3,
                            "source": 3,
                            "target": 0
                        }
                    ],
                    "nodes": 
                    [
                        {
                            "data": "a",
                            "edge_ids": [
                            ],
                            "id": 0
                        },
                        {
                            "data": "c",
                            "edge_ids": [
                                2
                            ],
                            "id": 2
                        },
                        {
                            "data": "d",
                            "edge_ids": [
                                3
                            ],
                            "id": 3
                        }
                    ]
                }
            }
        )");

        REQUIRE(g.to_json() == expected_json);
    }

    SECTION("Clear")
    {
        auto expected_json = nlohmann::json::parse(R"(
            {
                "graph": 
                {
                    "edges": [],
                    "nodes": []
                }
            }
        )");
    }

    SECTION("Add edge")
    {
        REQUIRE(g.add_edge(a, c, "a->c") == 6);
    }

    SECTION("Add undirected edge")
    {
        REQUIRE(g.add_undirected_edge(a, c, "a<->c") == std::pair<std::ptrdiff_t, std::ptrdiff_t>{6, 7});

        auto expected_json = nlohmann::json::parse(R"(
            {
                "graph": 
                {
                    "edges": 
                    [
                        {
                            "data": "a->b",
                            "id": 0,
                            "source": 0,
                            "target": 1
                        },
                        {
                            "data": "b->c",
                            "id": 1,
                            "source": 1,
                            "target": 2
                        },
                        {
                            "data": "c->d",
                            "id": 2,
                            "source": 2,
                            "target": 3
                        },
                        {
                            "data": "d->a",
                            "id": 3,
                            "source": 3,
                            "target": 0
                        },
                        {
                            "data": "b->d",
                            "id": 4,
                            "source": 1,
                            "target": 3
                        },
                        {
                            "data": "d->b",
                            "id": 5,
                            "source": 3,
                            "target": 1
                        },
                        {
                            "data": "a<->c",
                            "id": 6,
                            "source": 0,
                            "target": 2
                        },
                        {
                            "data": "a<->c",
                            "id": 7,
                            "source": 2,
                            "target": 0
                        }
                    ],
                    "nodes": 
                    [
                        {
                            "data": "a",
                            "edge_ids": [
                                0, 6
                            ],
                            "id": 0
                        },
                        {
                            "data": "b",
                            "edge_ids": [
                                1,
                                4
                            ],
                            "id": 1
                        },
                        {
                            "data": "c",
                            "edge_ids": [
                                2, 7
                            ],
                            "id": 2
                        },
                        {
                            "data": "d",
                            "edge_ids": [
                                3,
                                5
                            ],
                            "id": 3
                        }
                    ]
                }
            }
        )");

        REQUIRE(g.to_json() == expected_json);
    }

    SECTION("Get edge data")
    {
        REQUIRE(g.edge_data(ab) == "a->b");
    }

    SECTION("Get edge source")
    {
        REQUIRE(g.edge_source_vertex_id(ab) == a);
    }

    SECTION("Get edge target")
    {
        REQUIRE(g.edge_target_vertex_id(ab) == b);
    }

    SECTION("Remove edge")
    {
        g.remove_edge(bd);

        auto expected_json = nlohmann::json::parse(R"(
            {
                "graph": 
                {
                    "edges": 
                    [
                        {
                            "data": "a->b",
                            "id": 0,
                            "source": 0,
                            "target": 1
                        },
                        {
                            "data": "b->c",
                            "id": 1,
                            "source": 1,
                            "target": 2
                        },
                        {
                            "data": "c->d",
                            "id": 2,
                            "source": 2,
                            "target": 3
                        },
                        {
                            "data": "d->a",
                            "id": 3,
                            "source": 3,
                            "target": 0
                        },
                        {
                            "data": "d->b",
                            "id": 5,
                            "source": 3,
                            "target": 1
                        }
                    ],
                    "nodes": 
                    [
                        {
                            "data": "a",
                            "edge_ids": [
                                0
                            ],
                            "id": 0
                        },
                        {
                            "data": "b",
                            "edge_ids": [
                                1,
                                4
                            ],
                            "id": 1
                        },
                        {
                            "data": "c",
                            "edge_ids": [
                                2
                            ],
                            "id": 2
                        },
                        {
                            "data": "d",
                            "edge_ids": [
                                3,
                                5
                            ],
                            "id": 3
                        }
                    ]
                }
            }
        )");

        REQUIRE(g.to_json() == expected_json);
    }

    SECTION("Get amount vertices")
    {
        REQUIRE(g.amount_vertices() == 4);
    }

    SECTION("Get amount edges")
    {
        REQUIRE(g.amount_edges() == 6);
    }

    SECTION("Get vertex ids")
    {
        REQUIRE(g.vertex_ids() == std::set<std::ptrdiff_t>{a, b, c, d});
    }

    SECTION("Get edge ids")
    {
        REQUIRE(g.edge_ids() == std::set<std::ptrdiff_t>{ab, bc, cd, da, bd, db});
    }

    SECTION("Clear edges")
    {
        g.clear_edges();

        auto expected_json = nlohmann::json::parse(R"(
            {
                "graph": 
                {
                    "edges": [],
                    "nodes": 
                    [
                        {
                            "data": "a",
                            "edge_ids": [],
                            "id": 0
                        },
                        {
                            "data": "b",
                            "edge_ids": [],
                            "id": 1
                        },
                        {
                            "data": "c",
                            "edge_ids": [],
                            "id": 2
                        },
                        {
                            "data": "d",
                            "edge_ids": [],
                            "id": 3
                        }
                    ]
                }
            }
        )");

        REQUIRE(g.to_json() == expected_json);
    }

    SECTION("Transpose")
    {
        g.transpose();

        auto expected_json = nlohmann::json::parse(R"(
            {
                "graph": 
                {
                    "edges": 
                    [
                        {
                            "data": "a->b",
                            "id": 0,
                            "source": 1,
                            "target": 0
                        },
                        {
                            "data": "b->c",
                            "id": 1,
                            "source": 2,
                            "target": 1
                        },
                        {
                            "data": "c->d",
                            "id": 2,
                            "source": 3,
                            "target": 2
                        },
                        {
                            "data": "d->a",
                            "id": 3,
                            "source": 0,
                            "target": 3
                        },
                        {
                            "data": "b->d",
                            "id": 4,
                            "source": 3,
                            "target": 1
                        },
                        {
                            "data": "d->b",
                            "id": 5,
                            "source": 1,
                            "target": 3
                        }
                    ],
                    "nodes": 
                    [
                        {
                            "data": "a",
                            "edge_ids": [
                                3
                            ],
                            "id": 0
                        },
                        {
                            "data": "b",
                            "edge_ids": [
                                0,
                                5
                            ],
                            "id": 1
                        },
                        {
                            "data": "c",
                            "edge_ids": [
                                1
                            ],
                            "id": 2
                        },
                        {
                            "data": "d",
                            "edge_ids": [
                                2,
                                4
                            ],
                            "id": 3
                        }
                    ]
                }
            }
        )");

        REQUIRE(g.to_json() == expected_json);
    }

    SECTION("To JSON")
    {
        auto expected_json = nlohmann::json::parse(R"(
            {
                "graph": 
                {
                    "edges": 
                    [
                        {
                            "data": "a->b",
                            "id": 0,
                            "source": 0,
                            "target": 1
                        },
                        {
                            "data": "b->c",
                            "id": 1,
                            "source": 1,
                            "target": 2
                        },
                        {
                            "data": "c->d",
                            "id": 2,
                            "source": 2,
                            "target": 3
                        },
                        {
                            "data": "d->a",
                            "id": 3,
                            "source": 3,
                            "target": 0
                        },
                        {
                            "data": "b->d",
                            "id": 4,
                            "source": 1,
                            "target": 3
                        },
                        {
                            "data": "d->b",
                            "id": 5,
                            "source": 3,
                            "target": 1
                        }
                    ],
                    "nodes": 
                    [
                        {
                            "data": "a",
                            "edge_ids": [
                                0
                            ],
                            "id": 0
                        },
                        {
                            "data": "b",
                            "edge_ids": [
                                1,
                                4
                            ],
                            "id": 1
                        },
                        {
                            "data": "c",
                            "edge_ids": [
                                2
                            ],
                            "id": 2
                        },
                        {
                            "data": "d",
                            "edge_ids": [
                                3,
                                5
                            ],
                            "id": 3
                        }
                    ]
                }
            }
        )");

        REQUIRE(g.to_json() == expected_json);
    }

    SECTION("To DOT")
    {
        std::string expected_dot = R"(
            digraph {
                0 [label="(0) a"]
                1 [label="(1) b"]
                2 [label="(2) c"]
                3 [label="(3) d"]
                0 -> 1 [label="(0) a->b"]
                1 -> 2 [label="(1) b->c"]
                2 -> 3 [label="(2) c->d"]
                3 -> 0 [label="(3) d->a"]
                1 -> 3 [label="(4) b->d"]
                3 -> 1 [label="(5) d->b"]
            }
        )";

        std::string actual_dot = g.to_dot();

        std::regex whitespace(R"(\s+)");
        actual_dot = std::regex_replace(actual_dot, whitespace, "");
        expected_dot = std::regex_replace(expected_dot, whitespace, "");

        REQUIRE(actual_dot == expected_dot);
    }
}