// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "catch2/catch_test_macros.hpp"
#include "shuffle.hpp"
#include "simple-functions.hpp"

#include <algorithm>
#include <array>
#include <unordered_map>
#include <vector>

TEST_CASE("Shuffle vectors")
{
    std::vector<int> no_numbers = {};
    std::vector<int> one_number = {1337};
    std::vector<int> sorted_numbers = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    SECTION("Sort empty vector with naieve shuffle")
    {
        shuffle::naive(no_numbers.begin(), no_numbers.end());

        REQUIRE(no_numbers.empty());
    }

    SECTION("Sort vector with single element with naieve shuffle")
    {
        shuffle::naive(one_number.begin(), one_number.end());

        REQUIRE(one_number.size() == 1);
        REQUIRE(one_number[0] == 1337);
    }

    SECTION("Sort vector with naieve shuffle")
    {
        std::vector<int> original_array(sorted_numbers);

        shuffle::naive(sorted_numbers.begin(), sorted_numbers.end());

        REQUIRE(sorted_numbers != original_array);
        REQUIRE(std::is_permutation(sorted_numbers.begin(), sorted_numbers.end(), original_array.begin()));
    }

    SECTION("Sort empty vector with durstenfeld")
    {
        shuffle::durstenfeld(no_numbers.begin(), no_numbers.end());

        REQUIRE(no_numbers.empty());
    }

    SECTION("Sort vector with single element with durstenfeld")
    {
        shuffle::durstenfeld(one_number.begin(), one_number.end());

        REQUIRE(one_number.size() == 1);
        REQUIRE(one_number[0] == 1337);
    }

    SECTION("Sort vector with durstenfeld")
    {
        std::vector<int> original_array(sorted_numbers);

        shuffle::durstenfeld(sorted_numbers.begin(), sorted_numbers.end());

        REQUIRE(sorted_numbers != original_array);
        REQUIRE(std::is_permutation(sorted_numbers.begin(), sorted_numbers.end(), original_array.begin()));
    }
}

TEST_CASE("Test if Durstenfeld is unbiased")
{
    std::vector<int> numbers = {1, 2, 3};

    auto convert_vector_to_int = [](const std::vector<int>& v)
    {
        int result = 0;
        int radix = 1;
        int i = (v.size() - 1);
        while (i >= 0)
        {
            result += v[i] * radix;
            radix *= 10;
            i--;
        }
        return result;
    };

    REQUIRE(convert_vector_to_int(std::vector<int>{1, 2, 3, 4}) == 1234);
    REQUIRE(convert_vector_to_int(std::vector<int>{4, 3, 2, 1}) == 4321);

    std::unordered_map<int, int> count_permutations;

    do
    {
        count_permutations[convert_vector_to_int(numbers)] = 0;
    } while (std::next_permutation(numbers.begin(), numbers.end()));

    // The values here have been kept small to minimize testing time whilst
    // still being fairly effective detecting the difference between the naieve
    // shuffle and Durstenfeld. Better values would be 1'000'000 elements and an
    // 0.005 acceptable difference.
    //
    constexpr int amount_permutations = 6'000;
    const double average_amount_permutations = (static_cast<double>(amount_permutations)
                                                / static_cast<double>(simple_functions::factorial(numbers.size())));
    const double acceptable_difference = (0.11 * average_amount_permutations);

    for (int i = 0; i < amount_permutations; i++)
    {
        std::sort(numbers.begin(), numbers.end());
        shuffle::durstenfeld(numbers.begin(), numbers.end());
        count_permutations[convert_vector_to_int(numbers)]++;
    }

    for (const auto& result : count_permutations)
    {
        const double difference = std::abs(result.second - average_amount_permutations);
        REQUIRE(difference <= acceptable_difference);
    }
}