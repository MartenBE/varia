// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "binary-search-tree.hpp"
#include "catch2/catch_template_test_macros.hpp"

#include <list>
#include <map>
#include <unordered_map>

TEMPLATE_TEST_CASE("Test mapping structures",
                   "",
                   (std::map<int, int>),
                   (std::unordered_map<int, int>),
                   (binary_search_tree<int, int>) )
{
    std::vector<int> keys{50, 40, 60, 5, 55, 100, 0, 10, 90, 20, 70, 80};

    auto calculate_value = [](int key)
    {
        return (123 * key);
    };

    TestType map_under_test;

    for (const auto& k : keys)
    {
        map_under_test.insert({k, calculate_value(k)});
    }

    std::vector<int> new_keys{-10, 95, 120};
    int min_key = *std::min_element(keys.begin(), keys.end());
    int max_key = *std::max_element(keys.begin(), keys.end());
    REQUIRE(new_keys[0] < min_key);
    REQUIRE(new_keys[1] >= min_key);
    REQUIRE(new_keys[1] <= max_key);
    REQUIRE(new_keys[2] > max_key);

    SECTION("Default constructor")
    {
        TestType{};
    }

    SECTION("Initializer list constructor")
    {
        TestType temp{{50, calculate_value(50)},
                      {40, calculate_value(40)},
                      {60, calculate_value(60)},
                      {5, calculate_value(5)},
                      {55, calculate_value(55)},
                      {100, calculate_value(100)},
                      {0, calculate_value(0)},
                      {10, calculate_value(10)},
                      {90, calculate_value(90)},
                      {20, calculate_value(20)},
                      {70, calculate_value(70)},
                      {80, calculate_value(80)}};

        REQUIRE(temp == map_under_test);
    }

    SECTION("Copy constructor")
    {
        TestType temp{map_under_test};

        REQUIRE(temp == map_under_test);

        int last_key = keys[std::ssize(keys) - 1];
        temp.erase(temp.find(last_key));

        REQUIRE(temp != map_under_test);

        for (const auto& k : keys)
        {
            REQUIRE(map_under_test.at(k) == calculate_value(k));

            if (k != last_key)
            {
                REQUIRE(temp.at(k) == calculate_value(k));
            }
            else
            {
                REQUIRE(!temp.contains(k));
            }
        }
    }

    SECTION("Copy assignment")
    {
        TestType temp = map_under_test;

        REQUIRE(temp == map_under_test);

        int last_key = keys[std::ssize(keys) - 1];
        temp.erase(temp.find(last_key));

        REQUIRE(temp != map_under_test);

        for (const auto& k : keys)
        {
            REQUIRE(map_under_test.at(k) == calculate_value(k));

            if (k != last_key)
            {
                REQUIRE(temp.at(k) == calculate_value(k));
            }
            else
            {
                REQUIRE(!temp.contains(k));
            }
        }
    }

    SECTION("Move constructor")
    {
        TestType temp{std::move(map_under_test)};

        REQUIRE(temp != map_under_test);
        REQUIRE(map_under_test == TestType{});

        for (const auto& k : keys)
        {
            REQUIRE(temp.at(k) == calculate_value(k));
        }
    }

    SECTION("Move assignment")
    {
        TestType temp = std::move(map_under_test);

        REQUIRE(temp != map_under_test);
        REQUIRE(map_under_test == TestType{});

        for (const auto& k : keys)
        {
            REQUIRE(temp.at(k) == calculate_value(k));
        }
    }

    SECTION("Swap")
    {
        TestType temp1{{1, 111}, {2, 222}};
        TestType temp2{{3, 333}};

        std::swap(temp1, temp2);

        REQUIRE(std::ssize(temp1) == 1);
        REQUIRE(temp1[3] == 333);
        REQUIRE(!temp1.contains(1));
        REQUIRE(!temp1.contains(2));

        REQUIRE(std::ssize(temp2) == 2);
        REQUIRE(temp2[1] == 111);
        REQUIRE(temp2[2] == 222);
        REQUIRE(!temp2.contains(3));
    }

    SECTION("Is empty")
    {
        REQUIRE((TestType{}).empty());
        REQUIRE(!map_under_test.empty());
    }

    SECTION("Size")
    {
        REQUIRE(std::ssize(map_under_test) == std::ssize(keys));
    }

    SECTION("Clear")
    {
        map_under_test.clear();
        REQUIRE(map_under_test == TestType{});
        REQUIRE(std::ssize(map_under_test) == 0);
    }

    SECTION("At")
    {
        for (const auto& k : keys)
        {
            REQUIRE(map_under_test.at(k) == calculate_value(k));
        }
    }

    SECTION("Insert new keys")
    {
        for (const auto& k : new_keys)
        {
            int value = calculate_value(k);
            map_under_test.insert({k, value});
            REQUIRE(map_under_test.at(k) == value);
        }
    }

    SECTION("Erase")
    {
        TestType temp{{1, 111}};
        auto it = temp.find(1);
        temp.erase(it);

        REQUIRE(temp.empty());

        // 50: erase root node in bst
        // 80: erase node without children in bst
        // 70: erase node with only right child in bst
        // 40: erase node with only left child in bst
        //  5: erase node with two children where the right child is the successor in bst
        // 60: erase node with two children where the right child is not the successor in bst
        //
        for (const auto& key_to_remove : {50, 80, 70, 40, 5, 60})
        {
            REQUIRE(std::find(keys.begin(), keys.end(), key_to_remove) != keys.end());

            DYNAMIC_SECTION("Erase key " << key_to_remove)
            {
                auto it = map_under_test.find(key_to_remove);
                map_under_test.erase(it);

                REQUIRE(!map_under_test.contains(key_to_remove));
                REQUIRE(map_under_test.find(key_to_remove) == map_under_test.end());
                REQUIRE(std::ssize(map_under_test) == (std::ssize(keys) - 1));

                for (const auto& k : keys)
                {
                    if (k != key_to_remove)
                    {
                        REQUIRE(map_under_test.contains(k));

                        auto it = map_under_test.find(k);
                        REQUIRE(it != map_under_test.end());
                        REQUIRE(it->first == k);
                        REQUIRE(it->second == calculate_value(k));
                    }
                }
            }
        }
    }

    SECTION("retrieve existing keys with operator[]")
    {
        for (const auto& k : keys)
        {
            REQUIRE(map_under_test[k] == calculate_value(k));
        }
    }

    SECTION("Add new keys with operator[]")
    {
        for (const auto& k : new_keys)
        {
            int value = calculate_value(k);
            map_under_test[k] = value;
            REQUIRE(map_under_test[k] == value);
        }
    }

    SECTION("Find")
    {
        for (const auto& k : keys)
        {
            auto it = map_under_test.find(k);
            REQUIRE(it != map_under_test.end());
            REQUIRE(it->first == k);
            REQUIRE(it->second == calculate_value(k));
        }

        for (const auto& k : new_keys)
        {
            REQUIRE(map_under_test.find(k) == map_under_test.end());
        }
    }

    SECTION("Contains")
    {
        for (const auto& k : keys)
        {
            REQUIRE(map_under_test.contains(k));
        }

        for (const auto& k : new_keys)
        {
            REQUIRE(!map_under_test.contains(k));
        }
    }

    SECTION("Equals")
    {
        REQUIRE(TestType{} == TestType{});
        REQUIRE(map_under_test == map_under_test);

        REQUIRE(map_under_test != TestType{});
        REQUIRE(TestType{} != map_under_test);

        REQUIRE(TestType{{1, 111}, {2, 222}, {3, 333}} != TestType{{1, 111}, {2, 222}, {3, 555}});

        TestType temp{map_under_test};

        REQUIRE(map_under_test == temp);
        REQUIRE(temp == map_under_test);

        TestType temp_for_insert{map_under_test};

        int k = 1337;
        REQUIRE(std::find(keys.begin(), keys.end(), k) == keys.end());
        temp_for_insert.insert({k, calculate_value(k)});

        REQUIRE(map_under_test != temp_for_insert);
        REQUIRE(temp_for_insert != map_under_test);

        TestType temp_for_erase{map_under_test};

        auto it = temp_for_erase.find(keys[std::ssize(keys) / 2]);
        temp_for_erase.erase(it);

        REQUIRE(map_under_test != temp_for_erase);
        REQUIRE(temp_for_erase != map_under_test);
    }

    SECTION("Iterator")
    {
        std::unordered_map<int, int> freq_table;

        for (const auto& i : map_under_test)
        {
            freq_table[i.first] = i.second;
        }

        for (const auto& k : keys)
        {
            REQUIRE(freq_table.contains(k));
            REQUIRE(freq_table.at(k) == calculate_value(k));
        }
    }
}