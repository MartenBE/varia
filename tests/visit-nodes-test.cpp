// Copyright (C) 2020 Martijn Saelens
//
// SPDX-License-Identifier: MIT

#include "catch2/catch_test_macros.hpp"
#include "visit-nodes.hpp"

#include <vector>

TEST_CASE("Pre-order, postorder traversal (example wikipedia)")
{
    graph<std::string, int> gr;

    int a = gr.add_vertex("a");
    int b = gr.add_vertex("b");
    int c = gr.add_vertex("c");
    int d = gr.add_vertex("d");
    int e = gr.add_vertex("e");
    int f = gr.add_vertex("f");
    int g = gr.add_vertex("g");
    int h = gr.add_vertex("h");
    int i = gr.add_vertex("i");

    gr.add_edge(b, a, 1);
    gr.add_edge(b, d, 1);
    gr.add_edge(d, c, 1);
    gr.add_edge(d, e, 1);
    gr.add_edge(f, b, 1);
    gr.add_edge(f, g, 1);
    gr.add_edge(g, i, 1);
    gr.add_edge(i, h, 1);

    SECTION("Pre-order traversal")
    {
        std::vector<std::ptrdiff_t> preorder_vertex_ids = visit_nodes<std::string, int>::pre_order(gr, f);

        REQUIRE(preorder_vertex_ids == std::vector<std::ptrdiff_t>{f, b, a, d, c, e, g, i, h});
    }

    SECTION("Postorder traversal")
    {
        std::vector<std::ptrdiff_t> postorder_vertex_ids = visit_nodes<std::string, int>::post_order(gr, f);

        REQUIRE(postorder_vertex_ids == std::vector<std::ptrdiff_t>{a, c, e, d, b, h, i, g, f});
    }
}