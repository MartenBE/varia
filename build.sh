#!/usr/bin/env bash

# Copyright (C) 2020 Martijn Saelens
#
# SPDX-License-Identifier: MIT

### Settings and functions #####################################################

set -e
shopt -s extglob # Necessary for clean

function line {
    HEADER_WIDTH=80
    HEADER_CHAR="="
    printf "%${HEADER_WIDTH}s\n" " " | tr " " "${HEADER_CHAR}"
}

function header {
    echo
    line
    echo "$1"
    line
}

### Default values #############################################################

export CC=/usr/bin/clang
export CXX=/usr/bin/clang++
SOURCE_DIRS=("./include" "./src" "./tests")
BUILD_DIR="build"
FORMAT_DISABLED=false
LICENSE_CHECK_DISABLED=false
CLEAN_ENABLED=false
CMAKE_ARGUMENTS="-DCMAKE_BUILD_TYPE=Debug -DENABLE_CLANG_TIDY=ON"
TESTS_ENABLED=false
COVERAGE_ENABLED=false
VALGRIND_ENABLED=false
DOXYGEN_ENABLED=false

### Argument parsing ###########################################################

while [ -n "$1" ]
do
  case "${1}" in
    --clean)
        CLEAN_ENABLED=true
        ;;
    --coverage)
        CMAKE_ARGUMENTS="${CMAKE_ARGUMENTS} -DENABLE_COVERAGE=ON"
        COVERAGE_ENABLED=true
        ;;
    --no-format)
        FORMAT_DISABLED=true
        ;;
    --no-license-check)
        LICENSE_CHECK_DISABLED=true
        ;;
    --doxygen)
        DOXYGEN_ENABLED=true
        ;;
    --gcc)
        export CC=/usr/bin/gcc
        export CXX=/usr/bin/g++
        ;;
    --test)
        TESTS_ENABLED=true
        ;;
    --valgrind)
        VALGRIND_ENABLED=true
        ;;
    *)
        echo "Invalid option: -${1}."
        exit 2
        ;;
  esac
  shift
done

if { [ "${COVERAGE_ENABLED}" = true ] || [ "${VALGRIND_ENABLED}" = true ]; } && [ "${TESTS_ENABLED}" = false ] # SC2235
then
    echo "Error: --coverage or --valgrind can only be used when --test is set"
    exit 1
fi

echo "Source dirs: ${SOURCE_DIRS[*]}"
echo "Build dir: ${BUILD_DIR}"
echo "C++ compiler: ${CXX}"
echo "CMake arguments: ${CMAKE_ARGUMENTS}"
echo "Clean enabled: ${CLEAN_ENABLED}"
echo "Tests enabled: ${TESTS_ENABLED}"
echo "Coverage enabled: ${COVERAGE_ENABLED}"
echo "Doxygen enabled: ${DOXYGEN_ENABLED}"

### License check ##############################################################

if [ "${LICENSE_CHECK_DISABLED}" = false ]
then
    header "License check"
    reuse --version

    reuse lint
fi

### Formatting #################################################################

if [ "${FORMAT_DISABLED}" = false ]
then
    header "Formatting"
    clang-format --version

    for source_dir in "${SOURCE_DIRS[@]}"
    do
        find "${source_dir}" -regex '.*\.\(h\|hpp\|c\|cpp\)' -exec clang-format -i --verbose {} \;
    done
fi

### Clean and Build ############################################################

if [ -d "${BUILD_DIR}" ]
then
    if [ "${CLEAN_ENABLED}" = true ]
    then
        header "Clean"
        cd build
        rm -r -v !(_deps)
        cd -
    fi
else
    mkdir -p "${BUILD_DIR}"
fi

cd "${BUILD_DIR}"

header "CMake"
cmake --version

# shellcheck disable=SC2086
cmake ${CMAKE_ARGUMENTS} ..

header "Make"

make

### Testing ####################################################################

if [ "${TESTS_ENABLED}" = true ]
then
    header "Tests"

    TEST_EXECUTABLE="./tests/test"

    # shellcheck disable=SC2086
    ${TEST_EXECUTABLE}

    if [ "${COVERAGE_ENABLED}" = true ] ### Coverage ###########################
    then
        header "Coverage"
        llvm-profdata --version
        llvm-cov --version

        IGNORE_FILENAME_REGEX="(build/_deps|tests)/*"

        llvm-profdata merge -sparse default.profraw -o default.profdata
        llvm-cov report "${TEST_EXECUTABLE}" -instr-profile=default.profdata -ignore-filename-regex="${IGNORE_FILENAME_REGEX}"
        llvm-cov show "${TEST_EXECUTABLE}" -instr-profile=default.profdata -ignore-filename-regex="${IGNORE_FILENAME_REGEX}" --format=html > index.html
    fi

    if [ "${VALGRIND_ENABLED}" = true ] ### Valgrind ###########################
    then
        header "Valgrind"
        valgrind --version

        valgrind --tool=memcheck --leak-check=full --show-leak-kinds=all --track-origins=yes --show-error-list=yes --error-exitcode=1 "${TEST_EXECUTABLE}"
    fi
fi

### Documentation ##############################################################

if [ "${DOXYGEN_ENABLED}" = true ]
then
    header "Doxygen"
    doxygen --version

    cd docs
    doxygen Doxyfile
    cd ..
fi
